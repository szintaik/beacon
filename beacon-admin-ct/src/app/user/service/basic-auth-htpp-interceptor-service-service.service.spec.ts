import { TestBed } from '@angular/core/testing';

import { BasicAuthHtppInterceptorServiceServiceService } from './basic-auth-htpp-interceptor-service-service.service';

describe('BasicAuthHtppInterceptorServiceServiceService', () => {
  let service: BasicAuthHtppInterceptorServiceServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BasicAuthHtppInterceptorServiceServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
