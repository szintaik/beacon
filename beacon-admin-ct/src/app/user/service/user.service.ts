import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../model/user.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private baseUrl = 'http://localhost:8080/api/v1/user/';

  constructor(private httpClient: HttpClient) { }


  userRegistration(user: User): Observable<User>{

    return this.httpClient.post<User>(this.baseUrl+"registration", user);
  } 

  authenticate(username, password){
    return this.httpClient.post<any>('http://localhost:8080/api/v1/authenticate', {username, password}).pipe(
    map(
    userData =>{
      localStorage.setItem('username',username);
      let tokenStr= 'Bearer '+userData.token;
      localStorage.setItem('token', tokenStr);
      return userData;
    }

    ));
  }

  findActualUser(): Observable<User>{
    return this.httpClient.get<User>(this.baseUrl+"profile");
  }

  updateProfile(user: User, password: boolean): Observable<User>{
    return this.httpClient.put<User>(this.baseUrl+"?password="+password, user);
  }

  userToAdmin(user: User): Observable<User>{
    return this.httpClient.put<User>(this.baseUrl+"updateUserToAdmin", user);
  }

  isUserLoggedIn() {
    let user = localStorage.getItem('username')
    return !(user === null)
  }

  logOut() {
    localStorage.removeItem('username');
    localStorage.removeItem('token');
  }
}
