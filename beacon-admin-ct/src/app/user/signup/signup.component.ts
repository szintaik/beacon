import { Component, OnInit } from '@angular/core';
import { User } from '../model/user.model';
import { UserService } from '../service/user.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  userForm = this.fb.group({
    fullName: ['', [Validators.required]],
    email: ['', [Validators.required]],
    password: ['', [Validators.required]],
    confirmPassword: ['', [Validators.required]]
  });
  user: User;

  isPasswordsSame:boolean = true;



  test: Date = new Date();
  focus;
  focus1;
  focus2;
  constructor(private userService: UserService, private fb: FormBuilder, private router: Router) { }

  ngOnInit() { }


  registration() {

    this.user = new User();
    this.user.fullName = this.userForm.value.fullName;
    this.user.email = this.userForm.value.email;
    this.user.password = this.userForm.value.password;
    this.userService.userRegistration(this.user).subscribe(user => {

      if (user.id != null) {
        this.router.navigateByUrl('/login');
      }
    });

  }


  passwordCheck(event: any){

    if(this.userForm.value.password === this.userForm.value.confirmPassword){
      this.isPasswordsSame = true;
      this.userForm.controls['password'].setErrors(null);
    }
    else{
      this.userForm.controls['password'].setErrors({'incorrect': true});
      this.isPasswordsSame = false;
    }

  }


}

