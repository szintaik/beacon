import { Component, OnInit } from '@angular/core';
import { User } from '../model/user.model';
import { UserService } from '../service/user.service';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';

import * as jwt_decode from "jwt-decode";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  userForm = this.fb.group({
    email: [''],
    password: ['']
  });
  user: User;
  isLoginSuccessful: boolean = true;

  focus;
  focus1;
  constructor(private userService: UserService, private fb: FormBuilder, private router: Router) { }

  ngOnInit() {
  }

  login(){
    this.user = new User();
    this.user.email = this.userForm.value.email;
    this.user.password = this.userForm.value.password;

    this.userService.authenticate(this.user.email,this.user.password).subscribe(data =>{
      console.log(localStorage.getItem('token'));
      console.log(jwt_decode(localStorage.getItem('token')));
      this.router.navigateByUrl('/home');
    },
    err => {
      this.isLoginSuccessful = false;
    });
  }

}
