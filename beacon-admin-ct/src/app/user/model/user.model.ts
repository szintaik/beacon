import { Event } from "src/app/event/model/event.model";

export class User {

    id: number;
    email: string;
    fullName: string;
    password: string;
    events: Event[];

}
