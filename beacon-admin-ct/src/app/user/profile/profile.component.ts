import { Component, OnInit } from '@angular/core';
import { UserService } from '../service/user.service';
import { User } from '../model/user.model';

@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.scss']
})

export class ProfileComponent implements OnInit {

    user: User = new User();

    isDetailsEdit: boolean = false;

    isNewPasswordEdit: boolean = false;

    isPasswordsSame: boolean = true;


    constructor(private userService: UserService) { }

    ngOnInit() {
        this.getUser();
    }

    editProfile() {
        this.isDetailsEdit = !this.isDetailsEdit;
        if (this.isDetailsEdit) {
            (<HTMLInputElement>document.getElementById("email")).contentEditable = "true";
            (<HTMLInputElement>document.getElementById("fullName")).contentEditable = "true";
        }
        else {
            (<HTMLInputElement>document.getElementById("email")).contentEditable = "false";
            (<HTMLInputElement>document.getElementById("fullName")).contentEditable = "false";
        }
    }

    saveDetails() {
        this.isDetailsEdit = false;
        (<HTMLInputElement>document.getElementById("email")).contentEditable = "false";
        (<HTMLInputElement>document.getElementById("fullName")).contentEditable = "false";

        this.user.email = (<HTMLInputElement>document.getElementById("email")).textContent;
        this.user.fullName = (<HTMLInputElement>document.getElementById("fullName")).textContent;

        this.userService.updateProfile(this.user, false).subscribe(user => {
            this.user = user;
        });
        console.log(this.user)
    }


    passwordCheck(event: any) {

        let password = (<HTMLInputElement>document.getElementById("password")).value;
        let confirmPassword = (<HTMLInputElement>document.getElementById("confirmPassword")).value;
        console.log(password)

        if (password === confirmPassword) {
            this.isPasswordsSame = true;

        }
        else {

            this.isPasswordsSame = false;
        }

    }

    savePassword() {
        let password = (<HTMLInputElement>document.getElementById("password")).value;
        if (this.isPasswordsSame && password.length > 0) {
            this.isNewPasswordEdit = false;
            this.user.password = password;
            this.userService.updateProfile(this.user, true).subscribe(user => {
                this.user = user;
            });
        }
    }

    newPassword() {
        this.isNewPasswordEdit = !this.isNewPasswordEdit;
    }


    getUser() {

        this.userService.findActualUser().subscribe(user => this.user = user)
    }

}
