import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserToAdminComponent } from './user-to-admin/user-to-admin.component';



@NgModule({
  declarations: [UserToAdminComponent],
  imports: [
    CommonModule
  ]
})
export class AdminModule { }
