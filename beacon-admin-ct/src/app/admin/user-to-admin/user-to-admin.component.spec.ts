import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserToAdminComponent } from './user-to-admin.component';

describe('UserToAdminComponent', () => {
  let component: UserToAdminComponent;
  let fixture: ComponentFixture<UserToAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserToAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserToAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
