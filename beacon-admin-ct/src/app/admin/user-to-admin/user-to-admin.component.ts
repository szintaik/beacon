import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/user/service/user.service';
import { User } from 'src/app/user/model/user.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-to-admin',
  templateUrl: './user-to-admin.component.html',
  styleUrls: ['./user-to-admin.component.css']
})
export class UserToAdminComponent implements OnInit {



  constructor(private userService: UserService, private router: Router) { }

  ngOnInit(): void {

    this.userToAdmin();
  }





  userToAdmin(){
    this.userService.findActualUser().subscribe(user => {
      this.userService.userToAdmin(user).subscribe(user=>{
        this.router.navigateByUrl('/home');
      })
    })

  }




}
