import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateEventComponent } from './create-event/create-event.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CreateBeaconComponent } from './create-beacon/create-beacon.component';
import { ListEventsComponent } from './list-events/list-events.component';
import { UpdateEventComponent } from './update-event/update-event.component';
import { UpdateBeaconComponent } from './update-beacon/update-beacon.component';



@NgModule({
  declarations: [CreateEventComponent, CreateBeaconComponent, ListEventsComponent, UpdateEventComponent, UpdateBeaconComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class EventModule { }
