import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Image } from '../model/image.model';

@Injectable({
  providedIn: 'root'
})
export class ImageService {

  private baseUrl = 'http://localhost:8080/api/v1/image';
  
  headers: HttpHeaders;
  
  constructor(private httpClient: HttpClient) { }

  uploadImage(uploadImageData: FormData) : Observable<Image>{

    return this.httpClient.post<Image>(this.baseUrl, uploadImageData);
  }



}
