import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Beacon } from '../model/beacon.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BeaconService {

  private baseUrl = 'http://localhost:8080/api/v1/beacons';

  headers: HttpHeaders;


  constructor(private httpClient: HttpClient) { }

  updateBeacon(beacon: Beacon) : Observable<Beacon>{
    this.setHeader();
     return this.httpClient.put<Beacon>(this.baseUrl, beacon, { headers : this.headers });
   }

   getBeacon(id: number) : Observable<Beacon>{
    this.setHeader();
    return this.httpClient.get<Beacon>(`${this.baseUrl}/${id}`, { headers : this.headers });
  }

   setHeader(){

    this.headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': localStorage.getItem('token')
    })

  }
}
