import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Event } from '../model/event.model';


@Injectable({
  providedIn: 'root'
})
export class EventService {

  private baseUrl = 'http://localhost:8080/api/v1/events';

  headers: HttpHeaders;

  constructor(private httpClient: HttpClient) { }

  saveEvent(event: any): Observable<Event> {

    return this.httpClient.post<Event>(this.baseUrl, event);
  }

  addBeaconsToEvent(event: Event): Observable<Event> {

    return this.httpClient.post<Event>(this.baseUrl+"/addbeacons", event);
  }

  getEvents(): Observable<Event[]> {

    return this.httpClient.get<Event[]>(this.baseUrl);
  }

  deleteEvent(id: number): Observable<any> {

    return this.httpClient.delete<any>(`${this.baseUrl}/${id}`);
  }

}
