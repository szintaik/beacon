import { Component, OnInit } from '@angular/core';
import { EventService } from '../service/event.service';
import { ImageService } from '../service/image.service';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { Event } from '../model/event.model';

import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-update-event',
  templateUrl: './update-event.component.html',
  styleUrls: ['./update-event.component.css']
})
export class UpdateEventComponent implements OnInit {

  eventForm :any; 
  imageSrc: any;
  selectedFile: File;
  event: Event;

  logoURL: any;

  width: number = 0;
  height: number = 0;

  constructor(private eventService: EventService, private imageService: ImageService, private fb: FormBuilder, private router: Router, private sanitizer: DomSanitizer) { }


  ngOnInit(): void {
    this.getEvent();
  }

  getEvent(){

    this.event = JSON.parse(localStorage.getItem('event'));
    this.eventForm = this.fb.group({
      name: this.event.name,
      place: this.event.place,
      beaconTypes: this.event.beaconTypes
    });

    this.imageSrc = 'data:image/' + this.event.floorMap.type + ';base64,' + this.event.floorMap.picBase64;
 
    localStorage.setItem('imageSrc', JSON.stringify(this.imageSrc));

    this.logoURL = this.event.logoSrc;
    localStorage.setItem('logo',JSON.stringify(this.logoURL));

  }

  updateEvent() {


    this.router.navigateByUrl('/updatebeacon');

    /*
    this.event.name = this.eventForm.value.name;
    this.event.place = this.eventForm.value.place;
    const uploadImageData = new FormData();
    uploadImageData.append('imageFile', this.selectedFile);
    this.imageService.uploadImage(uploadImageData).subscribe(image => {
      this.event.floorMap = image; 
      this.event.floorMap.width = this.width;
      this.event.floorMap.height = this.height;
      this.eventService.saveEvent(this.event).subscribe(event => {
        localStorage.setItem('event',JSON.stringify(event));
       // this.router.navigateByUrl('/createbeacon');
      });
    });*/

  }


  public onFileChanged(event) {

    this.selectedFile = event.target.files[0];
    
    this.getImageWidthAndHeight(this.selectedFile);


    this.showImage(event.target.files[0]);
  }
  showImage(selectedFile: File) {
    var reader = new FileReader();
    reader.readAsDataURL(selectedFile);
    reader.onload = (_event) => {
      this.imageSrc = reader.result;
    }
  }

  
  getImageWidthAndHeight(file :any){
    let reader = new FileReader();

     

      const img = new Image();
      img.src = window.URL.createObjectURL( file );

      reader.readAsDataURL(file);
      reader.onload = () => {

        this.width = img.naturalWidth;
        this.height = img.naturalHeight;
        

        window.URL.revokeObjectURL( img.src );
        
    };


  }



}
