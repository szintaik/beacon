import { Event } from "./event.model";

export class Image {

    id: number;
    name: string;
    type: string;
    picByte: any;
    src: any;
    picBase64: any;
    width: number;
    height: number;
    event: Event;
}
