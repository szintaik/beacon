import { User } from "src/app/user/model/user.model";
import { Beacon } from "./beacon.model";
import { Image } from "./image.model";

export class Event {

    id: number;
    name: string;
    place: string;
    floorMap: Image;
    logoSrc: string;
    beaconsApiKey: string;
    user: User;
    beacons: Beacon[] = [];
    beaconTypes: string;
    completion: number;
    completionColor: string;
}
