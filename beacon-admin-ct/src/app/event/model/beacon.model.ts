import { Event } from "./event.model";
import { Sheet } from "src/app/sheet/model/sheet.model";

export class Beacon {

    id: number;
    name: string;
    event: Event;
    uniqueId: string;
    xCoordinate: number;
    yCoordinate: number;
    sheet: Sheet;
}
