import { Component, OnInit } from '@angular/core';
import { Event } from '../model/event.model';
import { Image } from '../model/image.model';
import { ImageService } from '../service/image.service';
import { Beacon } from '../model/beacon.model';
import { EventService } from '../service/event.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-beacon',
  templateUrl: './create-beacon.component.html',
  styleUrls: ['./create-beacon.component.css']
})
export class CreateBeaconComponent implements OnInit {

  event: Event;
  retrievedImage: Image = new Image();
  beacon: Beacon;

  beaconMarkers: any[] = [];
  beaconMarkerIdCounter: number = 0;
  logoURL : any;


  marker: any;
  markerClone: any;

  btnBeaconText: string = "Add new beacon";

  constructor(private imageService: ImageService, private eventService: EventService, private router: Router) { }

  ngOnInit(): void {
    this.marker = (<HTMLInputElement>document.getElementById("marker"));
    this.getImage();
  }

  setBeacon(event: any) {
       this.addBeacon();
      let yImgStart = window.scrollY + document.querySelector('#floormap').getBoundingClientRect().top; // y position
      let xImgStart = window.scrollX + document.querySelector('#floormap').getBoundingClientRect().left // X position

     
      this.beacon.xCoordinate = (event.pageX - xImgStart);
      this.beacon.yCoordinate = (event.pageY - yImgStart);


      this.markerClone.style.left = (event.pageX - 12.5) + 'px'; // or event.clientX
      this.markerClone.style.top = (event.pageY - 25) + 'px';   // or event.clientY
      this.markerClone.style.display = "block";
      

  }

  addBeacon() {
     
      this.beacon = new Beacon();

     
      this.beacon.name = "NAME";
      this.beacon.uniqueId = "UNIQUEID";

      this.markerClone = <HTMLInputElement>(this.marker.cloneNode(true));
      (<HTMLInputElement>document.getElementById("beaconMarkers")).append(this.markerClone);

      this.beacon.id = this.beaconMarkerIdCounter;
      this.markerClone.id = "marker" + this.beaconMarkerIdCounter++;
      
      this.event.beacons.push(this.beacon);


  }

  nextStep() {

    this.event.beacons.forEach(beacon => beacon.id = null);

    this.eventService.addBeaconsToEvent(this.event).subscribe(event => {
      localStorage.setItem('event', JSON.stringify(event));
      this.router.navigateByUrl('/addsheet');
    })
  }

  removeBeacon(id: number) {
    (<HTMLInputElement>document.getElementById("marker" + id)).remove();

    let beaconToRemove: Beacon;

    this.event.beacons.forEach(beacon => {
      if (beacon.id == id) beaconToRemove = beacon;
    });

    let index: number = this.event.beacons.indexOf(beaconToRemove);
    this.event.beacons.splice(index, 1);

  }



  getImage() {

    this.event = JSON.parse(localStorage.getItem('event'));
    this.logoURL = JSON.parse(localStorage.getItem('logo'));


    this.retrievedImage.src = 'data:image/' + this.event.floorMap.type + ';base64,' + this.event.floorMap.picByte;
    localStorage.setItem('imageSrc', JSON.stringify(this.retrievedImage.src));
    
  }


  updateName(beacon: Beacon, event: any){
    
    this.event.beacons.forEach(b => {
      if(b.id === beacon.id){
        b.name = event.target.textContent;
      }
    })
  }

  updateUniqueId(beacon: Beacon, event: any){

    this.event.beacons.forEach(b => {
      if(b.id === beacon.id){
        b.uniqueId = event.target.textContent;
      }
    })
    

  }

}
