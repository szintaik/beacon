import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateBeaconComponent } from './create-beacon.component';

describe('CreateBeaconComponent', () => {
  let component: CreateBeaconComponent;
  let fixture: ComponentFixture<CreateBeaconComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateBeaconComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateBeaconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
