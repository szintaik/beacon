import { Component, OnInit } from '@angular/core';
import { Event } from '../model/event.model';
import { EventService } from '../service/event.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-events',
  templateUrl: './list-events.component.html',
  styleUrls: ['./list-events.component.css']
})
export class ListEventsComponent implements OnInit {

  events: Event[];

  constructor(private eventService: EventService,  private router: Router) { }

  ngOnInit(): void {
    this.getEvents();

  }
  setCompletion() {
    let completion;

    this.events.forEach(event =>{
    completion = 40;
    
    if(event.beacons.length > 0){
      completion += 20;
    }
  let beaconsLength = event.beacons.length;
    event.beacons.forEach(beacon => {
      if(beacon.sheet != null){
      completion += 40/beaconsLength;
    }
    })

    
    event.completion = completion.toFixed(2);
    if(completion < 60){
      event.completionColor = "progress-bar bg-danger"; 
    }
    if(completion >= 60 && completion <= 80){
      event.completionColor = "progress-bar bg-warning"; 
    }
    if(completion > 80 && completion < 100){
      event.completionColor = "progress-bar bg-info"; 
    }
    if(completion > 99){
      event.completionColor = "progress-bar bg-success"; 
    }
    });


  }

  deleteEvent(id: number) {
    this.eventService.deleteEvent(id).subscribe(data => {
      location.reload();
    }
    );
  }

  updateEvent(event: Event){
    localStorage.setItem('event',JSON.stringify(event));
    this.router.navigateByUrl('/updateevent');
  }

  getEvents() {

    this.eventService.getEvents().subscribe(events => {
    events.forEach(e => {
      if(e.logoSrc != null){
        e.logoSrc = 'data:image/jpg;base64,'+ e.logoSrc;
      }
    })
    this.events = events;
    this.setCompletion();
    });

  }

}
