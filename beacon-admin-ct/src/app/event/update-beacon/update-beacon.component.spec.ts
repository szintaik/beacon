import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateBeaconComponent } from './update-beacon.component';

describe('UpdateBeaconComponent', () => {
  let component: UpdateBeaconComponent;
  let fixture: ComponentFixture<UpdateBeaconComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateBeaconComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateBeaconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
