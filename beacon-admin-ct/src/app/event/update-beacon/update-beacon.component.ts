import { Component, OnInit } from '@angular/core';
import { Image } from '../model/image.model';
import { Beacon } from '../model/beacon.model';
import { Event } from '../model/event.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-update-beacon',
  templateUrl: './update-beacon.component.html',
  styleUrls: ['./update-beacon.component.css']
})
export class UpdateBeaconComponent implements OnInit {

  event: Event;
  retrievedImage: Image = new Image();
  beacon: Beacon;

  beaconMarkers: any[] = [];
  beaconMarkerIdCounter: number = 0;

  logoURL: any;

  marker: any;
  markerClone: any;

  constructor(private router: Router) { }

  ngOnInit(): void {
    this.getEvent();
  }


  getEvent() {

    this.event = JSON.parse(localStorage.getItem('event'));
    this.retrievedImage.src = JSON.parse(localStorage.getItem('imageSrc'));
    this.logoURL = JSON.parse(localStorage.getItem('logo'));
    this.setBeaconMarkers();
  }

  nextStep() {

    this.router.navigateByUrl('/updateaddsheet');

    /*   this.event.beacons.forEach(beacon => beacon.id = null);
   
       this.eventService.addBeaconsToEvent(this.event).subscribe(event => {
         localStorage.setItem('event', JSON.stringify(event));
         this.router.navigateByUrl('/addsheet');
       })*/
  }



  setBeaconMarkers() {
    this.marker = (<HTMLInputElement>document.getElementById("marker"));
    let yImgStart = window.scrollY + document.querySelector('#floormap').getBoundingClientRect().top; // y position
    let xImgStart = window.scrollX + document.querySelector('#floormap').getBoundingClientRect().left // X position

    let pageX = 0;
    let pageY = 0;



    this.event.beacons.forEach(beacon => {

      this.markerClone = <HTMLInputElement>(this.marker.cloneNode(true));
      (<HTMLInputElement>document.getElementById("beaconMarkers")).append(this.markerClone);


      this.markerClone.id = "marker" + beacon.id;

      pageX = beacon.xCoordinate + xImgStart;
      pageY = beacon.yCoordinate + yImgStart;

      this.markerClone.style.left = (pageX - 12.5) + 'px';
      this.markerClone.style.top = (pageY - 25) + 'px';
      this.markerClone.style.display = "block";



    });




  }

}
