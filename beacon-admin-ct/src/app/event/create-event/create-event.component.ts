import { Component, OnInit } from '@angular/core';
import { Event } from '../model/event.model';
import { FormBuilder } from '@angular/forms';
import { EventService } from '../service/event.service';
import { ImageService } from '../service/image.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-event',
  templateUrl: './create-event.component.html',
  styleUrls: ['./create-event.component.css']
})
export class CreateEventComponent implements OnInit {

  eventForm = this.fb.group({
    name: [''],
    place:[''],
    beaconTypes:[''],
    beaconApiKey:['']
  });
  event: Event = new Event();
  selectedFile: File;
  imgURL: any;
  logoURL: any;

  width: number = 0;
  height: number = 0;

  constructor(private eventService: EventService, private imageService: ImageService, private fb: FormBuilder, private router: Router) { }

  ngOnInit(): void {
  }

  public onFileChanged(event) {

    this.selectedFile = event.target.files[0];
    
    this.getImageWidthAndHeight(event.target.files[0]);


    this.showImage(event.target.files[0]);
  }
  showImage(selectedFile: File) {
    var reader = new FileReader();

    reader.readAsDataURL(selectedFile);
    reader.onload = (_event) => {
      this.imgURL = reader.result;
     
    }
  }

  createEvent() {

    this.event.name = this.eventForm.value.name;
    this.event.place = this.eventForm.value.place;
    this.event.beaconTypes = this.eventForm.value.beaconTypes;
    this.event.beaconsApiKey = this.eventForm.value.beaconApiKey;
    this.event.logoSrc = this.logoURL;
    localStorage.setItem('logo',JSON.stringify(this.logoURL));
    
    const uploadImageData = new FormData();
    uploadImageData.append('imageFile', this.selectedFile);
    this.imageService.uploadImage(uploadImageData).subscribe(image => {

      localStorage.setItem('sheetSrc',image.id+"_images");

      this.event.floorMap = image; 
      this.event.floorMap.width = this.width;
      this.event.floorMap.height = this.height;
      this.eventService.saveEvent(this.event).subscribe(event => {
        localStorage.setItem('event',JSON.stringify(event));
        this.router.navigateByUrl('/createbeacon');
      });
    });

  }

  getImageWidthAndHeight(file :any){
    let reader = new FileReader();

     

      const img = new Image();
      img.src = window.URL.createObjectURL( file );

      reader.readAsDataURL(file);
      reader.onload = () => {

        this.width = img.naturalWidth;
        this.height = img.naturalHeight;
        console.log(this.width + " " +this.height)

        window.URL.revokeObjectURL( img.src );
        
    };


  }

  public onFileChangedLogo(event) {
    console.log(this.width + " " +this.height)
    this.showLogo(event.target.files[0]);
  }

  showLogo(selectedFile: File) {
    var reader = new FileReader();
    
    reader.readAsDataURL(selectedFile);
    reader.onload = (_event) => {
      this.logoURL = reader.result;

    }
  }




}
