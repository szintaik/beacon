import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule  } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './user/profile/profile.component';
import { SignupComponent } from './user/signup/signup.component';
import { LandingComponent } from './landing/landing.component';
import { LoginComponent } from './user/login/login.component';
import { CreateEventComponent } from './event/create-event/create-event.component';
import { CreateBeaconComponent } from './event/create-beacon/create-beacon.component';
import { CreateSheetComponent } from './sheet/create-sheet/create-sheet.component';
import { AddSheetComponent } from './sheet/add-sheet/add-sheet.component';
import { ListEventsComponent } from './event/list-events/list-events.component';
import { UpdateEventComponent } from './event/update-event/update-event.component';
import { UpdateBeaconComponent } from './event/update-beacon/update-beacon.component';
import { UpdateAddSheetComponent } from './sheet/update-add-sheet/update-add-sheet.component';
import { UpdateSheetComponent } from './sheet/update-sheet/update-sheet.component';
import { UserToAdminComponent } from './admin/user-to-admin/user-to-admin.component';


const routes: Routes =[
    { path: 'home',             component: HomeComponent },
    { path: 'user-profile',     component: ProfileComponent },
    { path: 'register',           component: SignupComponent },
    { path: 'landing',          component: LandingComponent },
    { path: 'login',          component: LoginComponent },
    { path: 'createevent',          component: CreateEventComponent },
    { path: 'createbeacon',          component: CreateBeaconComponent },
    { path: 'addsheet',          component: AddSheetComponent },
    { path: 'createsheet',          component: CreateSheetComponent },
    { path: 'listevents',          component: ListEventsComponent },
    { path: 'updateevent',          component: UpdateEventComponent },
    { path: 'updatebeacon',          component: UpdateBeaconComponent },
    { path: 'updateaddsheet',          component: UpdateAddSheetComponent },
    { path: 'updatesheet',          component: UpdateSheetComponent },
    { path: 'usertoadmin',          component: UserToAdminComponent },
    { path: '', redirectTo: 'home', pathMatch: 'full' }
];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
  ],
})
export class AppRoutingModule { }
