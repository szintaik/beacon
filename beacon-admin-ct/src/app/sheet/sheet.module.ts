import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateSheetComponent } from './create-sheet/create-sheet.component';
import { AddSheetComponent } from './add-sheet/add-sheet.component';
import { UpdateAddSheetComponent } from './update-add-sheet/update-add-sheet.component';
import { UpdateSheetComponent } from './update-sheet/update-sheet.component';



@NgModule({
  declarations: [CreateSheetComponent, AddSheetComponent, UpdateAddSheetComponent, UpdateSheetComponent],
  imports: [
    CommonModule
  ]
})
export class SheetModule { }
