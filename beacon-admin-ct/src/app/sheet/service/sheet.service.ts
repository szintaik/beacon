import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Sheet } from '../model/sheet.model';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SheetService {
  private baseUrl = 'http://localhost:8080/api/v1/sheets';

  constructor(private httpClient: HttpClient) { }

  

  getSheet(id: number) : Observable<Sheet>{

    return this.httpClient.get<Sheet>(`${this.baseUrl}/${id}`);
  }


  getSheets() : Observable<Sheet[]>{

    return this.httpClient.get<Sheet[]>(this.baseUrl);
  }

  createSheet(sheet: Sheet) : Observable<Sheet>{
   
     return this.httpClient.post<Sheet>(this.baseUrl, sheet);
   }

   editSheet(sheet: Sheet, id: number) : Observable<Sheet>{
    
     return this.httpClient.put<Sheet>(`${this.baseUrl}/${id}`, sheet);
   }

   deleteSheet(id : number) : Observable<any>{

    return this.httpClient.delete(`${this.baseUrl}/${id}`);
  }

  
}
