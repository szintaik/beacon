import { Component, OnInit } from '@angular/core';
import { Image } from 'src/app/event/model/image.model';
import { Event } from 'src/app/event/model/event.model';
import { Router } from '@angular/router';
import { Beacon } from 'src/app/event/model/beacon.model';
import { Sheet } from '../model/sheet.model';
import { ColumnType } from '../model/column-type.enum';

@Component({
  selector: 'app-update-sheet',
  templateUrl: './update-sheet.component.html',
  styleUrls: ['./update-sheet.component.css']
})
export class UpdateSheetComponent implements OnInit {

  event: Event;

  beacon: Beacon;
  sheet: Sheet;
  valueId: number = 0;
  columnsRowCounter: number = 0;
  columsRowCounterArray: number[] = [];
  isColumnFinal: boolean = false;
  logoURL: any;

  constructor(private router: Router) { }

  ngOnInit(): void {
    this.getSheet();
  }

  getSheet(){

    this.beacon = JSON.parse(localStorage.getItem('beacon'));
    this.sheet = this.beacon.sheet;

    this.sheet.columns.forEach(column =>{ 
      if(column.type == ColumnType.IMAGE){ 
      column.values.forEach(value => value.value = 'data:image/jpg;base64,' + value.value)
    }
    })
    console.log(this.sheet)

    this.logoURL = JSON.parse(localStorage.getItem('logo'));
  }

  finish() {

   
        this.router.navigateByUrl('/updateaddsheet');


  }

}
