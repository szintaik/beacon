import { Component, OnInit } from '@angular/core';
import { Image } from 'src/app/event/model/image.model';
import { Event } from 'src/app/event/model/event.model';
import { ImageService } from 'src/app/event/service/image.service';
import { EventService } from 'src/app/event/service/event.service';
import { Router } from '@angular/router';
import { Beacon } from 'src/app/event/model/beacon.model';

@Component({
  selector: 'app-add-sheet',
  templateUrl: './add-sheet.component.html',
  styleUrls: ['./add-sheet.component.css']
})
export class AddSheetComponent implements OnInit {

  event: Event;
  retrievedImage: Image = new Image();
  logoURL: any;

  marker: any;
  markerClone: any;

  constructor(private imageService: ImageService, private eventService: EventService, private router: Router) { }

  ngOnInit(): void {
    this.marker = (<HTMLInputElement>document.getElementById("marker"));
    this.getImage();
  }



  getImage() {
    this.event = JSON.parse(localStorage.getItem('event'));

    this.retrievedImage.src = JSON.parse(localStorage.getItem('imageSrc'));

    this.logoURL = JSON.parse(localStorage.getItem('logo'));

    this.setBeaconMarkers();

  }

  setBeaconMarkers() {

    let yImgStart = window.scrollY + document.querySelector('#floormap').getBoundingClientRect().top; // y position
    let xImgStart = window.scrollX + document.querySelector('#floormap').getBoundingClientRect().left // X position
   
    this.event.beacons.forEach(beacon => { 

      this.markerClone = <HTMLInputElement>(this.marker.cloneNode(true));
      (<HTMLInputElement>document.getElementById("beaconMarkers")).append(this.markerClone);

      this.markerClone.id = "marker" + beacon.id;
      this.markerClone.style.left = (beacon.xCoordinate + xImgStart - 12.5 +1) + 'px';
      this.markerClone.style.top = (beacon.yCoordinate + yImgStart - 25 +1) + 'px';
      this.markerClone.style.display = "block";

    });


  }

  addSheet(beacon: Beacon) {
    localStorage.setItem('beacon', JSON.stringify(beacon));
    this.router.navigateByUrl('/createsheet');
  }

  finish(){
    this.router.navigateByUrl('/listevents');
  }
}
