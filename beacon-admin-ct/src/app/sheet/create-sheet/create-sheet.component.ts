import { Component, OnInit } from '@angular/core';
import { Image } from 'src/app/event/model/image.model';
import { ImageService } from 'src/app/event/service/image.service';
import { EventService } from 'src/app/event/service/event.service';
import { Router } from '@angular/router';
import { Beacon } from 'src/app/event/model/beacon.model';
import { Column } from '../model/column.model';
import { Sheet } from '../model/sheet.model';
import { Value } from '../model/value.model';
import { BeaconService } from 'src/app/event/service/beacon.service';
import { SheetService } from '../service/sheet.service';
import { ColumnType } from '../model/column-type.enum';


@Component({
  selector: 'app-create-sheet',
  templateUrl: './create-sheet.component.html',
  styleUrls: ['./create-sheet.component.css']
})
export class CreateSheetComponent implements OnInit {

  event: Event;

  beacon: Beacon;
  sheet: Sheet;
  valueId: number = 0;
  columnsRowCounter: number = 0;
  columsRowCounterArray: number[] = [];
  isColumnFinal: boolean = false;

  logoURL: any;


  constructor(private sheetService: SheetService, private beaconService: BeaconService, private router: Router) { }

  ngOnInit(): void {
    this.beacon = JSON.parse(localStorage.getItem('beacon'));

    this.logoURL = JSON.parse(localStorage.getItem('logo'));

    this.sheet = new Sheet();
  }


  addColumn(columnType: any) {

    let columnName = "COLUMN"
    let column = new Column();
    column.name = columnName;
    column.type = (columnType == 0) ? ColumnType.TEXT : ColumnType.IMAGE;
    this.sheet.columns.push(column);
  }


  addValues() {

    for (let i = 0; i < this.sheet.columns.length; i++) {
      let value = new Value();
      value.id = this.valueId++;
      if (this.sheet.columns[i].type == 0) {
        value.value = value.id + "test";
      }
      this.sheet.columns[i].values.push(value);

    }

    this.columsRowCounterArray.push(this.columnsRowCounter);
    this.columnsRowCounter++;
  }

  deleteLine() {
    this.sheet.columns.forEach((columns) => columns.values.splice(columns.values.length - 1, 1));

  }

  deleteColumn(){

    this.sheet.columns.splice(this.sheet.columns.length - 1, 1);
  }

  updateValue(lineId: number, event: any) {

    this.sheet.columns.forEach((column) =>
      column.values.forEach((value) => {
        if (value.id === lineId) {
          value.value = event.target.textContent
        };
      }
      ))

  }


  updateColumnName(columnIndex: number, event: any) {

    this.sheet.columns[columnIndex].name = event.target.textContent;

  }


  isColumnsFinal() {
    this.isColumnFinal = true;
  }

  finish() {

    this.sheet.columns.forEach((column) => {
      column.values.forEach((value) => {
        value.id = null;
      })
    }
    );

   
    this.sheet.src = localStorage.getItem('sheetSrc');
    console.log(this.sheet)
    this.sheetService.createSheet(this.sheet).subscribe(sheet => {
      this.beacon.sheet = sheet;
      this.beaconService.updateBeacon(this.beacon).subscribe(beacon =>
        this.router.navigateByUrl('/addsheet'));
    });

  }

  public onFileChanged(event, valueId: number) {

    this.showImage(event.target.files[0], valueId);
  }
  showImage(selectedFile: File, valueId: number) {
    var reader = new FileReader();
    let src;
    reader.readAsDataURL(selectedFile);
    reader.onload = (_event) => {
     src = reader.result;
     this.sheet.columns.forEach((column) => {
      column.values.forEach((value) => {
       if(value.id == valueId){
         value.value = src;
       }
      })
    }
    );
    }
    

  }

}
