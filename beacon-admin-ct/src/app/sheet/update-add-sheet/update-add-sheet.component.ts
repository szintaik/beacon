import { Component, OnInit } from '@angular/core';
import { Image } from 'src/app/event/model/image.model';
import { Event } from 'src/app/event/model/event.model';
import { Router } from '@angular/router';
import { Beacon } from 'src/app/event/model/beacon.model';


@Component({
  selector: 'app-update-add-sheet',
  templateUrl: './update-add-sheet.component.html',
  styleUrls: ['./update-add-sheet.component.css']
})
export class UpdateAddSheetComponent implements OnInit {

  event: Event;
  retrievedImage: Image = new Image();
  logoURL: any;

  marker: any;
  markerClone: any;

  constructor(private router: Router) { }

  ngOnInit(): void {

    this.getImage();
  }

  getImage() {
    this.event = JSON.parse(localStorage.getItem('event'));
   
    this.retrievedImage.src = JSON.parse(localStorage.getItem('imageSrc'));

    this.logoURL = JSON.parse(localStorage.getItem('logo'));

    this.setBeaconMarkers();

  }

  setBeaconMarkers() {
    this.marker = (<HTMLInputElement>document.getElementById("marker"));
    let yImgStart = window.scrollY + document.querySelector('#floormap').getBoundingClientRect().top; // y position
    let xImgStart = window.scrollX + document.querySelector('#floormap').getBoundingClientRect().left // X position
    console.log(yImgStart)
    this.event.beacons.forEach(beacon => {

      this.markerClone = <HTMLInputElement>(this.marker.cloneNode(true));
      (<HTMLInputElement>document.getElementById("beaconMarkers")).append(this.markerClone);

      this.markerClone.id = "marker" + beacon.id;
      this.markerClone.style.left = (beacon.xCoordinate + xImgStart - 12.5 + 1) + 'px';
      this.markerClone.style.top = (beacon.yCoordinate + yImgStart - 25 + 1) + 'px';
      this.markerClone.style.display = "block";


    });
  }

  updateSheet(beacon: Beacon) {
    localStorage.setItem('beacon', JSON.stringify(beacon));
    this.router.navigateByUrl('/updatesheet');
  }

  finish() {
    this.router.navigateByUrl('/listevents');
  }


}
