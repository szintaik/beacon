import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateAddSheetComponent } from './update-add-sheet.component';

describe('UpdateAddSheetComponent', () => {
  let component: UpdateAddSheetComponent;
  let fixture: ComponentFixture<UpdateAddSheetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateAddSheetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateAddSheetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
