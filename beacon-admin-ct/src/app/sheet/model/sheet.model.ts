import { Column } from "./column.model";

export class Sheet {

    id: number;
    src: string;
    columns: Column[] = [];
}
