import { Column } from "./column.model";

export class Value {

    id: number;
    tableId: number;
    value: string;
    column: Column;
}
