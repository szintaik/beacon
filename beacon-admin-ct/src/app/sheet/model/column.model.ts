import { Sheet } from "./sheet.model";
import { Value } from "./value.model";
import { ColumnType } from "./column-type.enum";

export class Column {

    id: number;
    name: string;
    sheet: Sheet;
    values: Value[]=[];
    type: ColumnType;
}
