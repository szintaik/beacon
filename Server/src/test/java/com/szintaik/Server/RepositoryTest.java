package com.szintaik.Server;

import com.szintaik.Server.model.*;
import com.szintaik.Server.repository.EventRepository;
import com.szintaik.Server.repository.RoleRepository;
import com.szintaik.Server.repository.UserRepository;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.*;

import org.springframework.transaction.annotation.Transactional;
import org.testcontainers.containers.PostgreSQLContainer;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(initializers = {RepositoryTest.Initializer.class})
public class RepositoryTest {


    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private EventRepository eventRepository;

    private User user;
    private String ROLE_USER;
    private String USER_EMAIL;
    private String USER_FULLNAME;
    private String USER_PASSWORD;

    private String EVENT_NAME;
    private String EVENT_PLACE;

    private String BEACON_NAME;
    private String BEACON_UNIQUEID;

    private String COLUMN1_NAME;
    private String COLUMN2_NAME;

    private String VALUE1;
    private String VALUE2;
    private String VALUE3;
    private String VALUE4;



    @ClassRule
    public static PostgreSQLContainer postgreSQLContainer = new PostgreSQLContainer("postgres:11.1")
            .withDatabaseName("BeaconServerTest")
            .withUsername("postgres")
            .withPassword("password");

    static class Initializer
            implements ApplicationContextInitializer<ConfigurableApplicationContext> {
        public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
            TestPropertyValues.of(
                    "spring.datasource.url=" + postgreSQLContainer.getJdbcUrl(),
                    "spring.datasource.username=" + postgreSQLContainer.getUsername(),
                    "spring.datasource.password=" + postgreSQLContainer.getPassword()
            ).applyTo(configurableApplicationContext.getEnvironment());
        }


    }

    @Before
    public void init() {

        ROLE_USER = "ROLE_USER";
        USER_EMAIL = "test@email.com";
        USER_FULLNAME = "testFullname";
        USER_PASSWORD = "testPassword";

        EVENT_NAME = "testEventName";
        EVENT_PLACE = "testEventPlace";

        BEACON_NAME = "testBeaconName";
        BEACON_UNIQUEID = "testBeaconUniqueId";

        COLUMN1_NAME = "testColumn1Name";
        COLUMN2_NAME = "testColumn2Name";

        VALUE1 = "testValue1";
        VALUE2 = "testValue2";
        VALUE3 = "testValue3";
        VALUE4 = "testValue4";

        initUserWithRole();
        initEvent();
    }

    @Test
    @Transactional
    public void saveUserTest() {
        User user;
        Role role;

        user = userRepository.findByEmail(USER_EMAIL);
        role = roleRepository.findByRole(ROLE_USER);

        assertThat(role.getRole()).isEqualTo(ROLE_USER);
        assertThat(user.getEmail()).isEqualTo(USER_EMAIL);
        assertThat(user.getRole().getRole()).isEqualTo(ROLE_USER);
    }

    @Test
    @Transactional
    public void testEvent() {

        Event event = eventRepository.findByName(EVENT_NAME);

        Beacon beacon = event.getBeacons().get(0);

        Column column = beacon.getSheet().getColumns().get(0);

        Value value = column.getValues().get(0);

        assertThat(event.getBeacons().isEmpty()).isFalse();

        assertThat(beacon.getSheet()).isNotNull();

        assertThat(beacon.getSheet().getColumns().isEmpty()).isFalse();


        assertThat(column.getValues().isEmpty()).isFalse();

        assertThat(value.getValue().equals(VALUE1)).isTrue();


    }


    private void initUserWithRole() {

        roleRepository.save(new Role(ROLE_USER));

        user = new User(USER_EMAIL, USER_FULLNAME, USER_PASSWORD);

        user.setRole(roleRepository.findByRole(ROLE_USER));

        userRepository.save(user);

        userRepository.flush();
    }


    private void initEvent(){

        Event event = new Event(EVENT_NAME,EVENT_PLACE,user);

        Beacon beacon = new Beacon(BEACON_NAME, event, BEACON_UNIQUEID);

        Sheet sheet = new Sheet();

        Column column1 = new Column(COLUMN1_NAME, ColumnType.TEXT, sheet);
        Column column2 = new Column(COLUMN2_NAME, ColumnType.TEXT, sheet);

        Value value1 = new Value(VALUE1, column1);
        Value value2 = new Value(VALUE2, column1);
        Value value3 = new Value(VALUE3, column2);
        Value value4 = new Value(VALUE4, column2);

        List<Value> values1 = new ArrayList<>();
        List<Value> values2 = new ArrayList<>();

        values1.add(value1);
        values1.add(value2);
        values2.add(value3);
        values2.add(value4);

        column1.setValues(values1);
        column2.setValues(values2);

        List<Column> columns = new ArrayList<>();

        columns.add(column1);
        columns.add(column2);

        sheet.setColumns(columns);

        beacon.setSheet(sheet);

        List<Beacon> beacons = new ArrayList<>();

        beacons.add(beacon);

        event.setBeacons(beacons);

        eventRepository.save(event);



    }

}
