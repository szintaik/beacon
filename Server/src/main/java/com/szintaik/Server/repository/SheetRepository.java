package com.szintaik.Server.repository;

import com.szintaik.Server.model.Sheet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SheetRepository extends JpaRepository<Sheet, Long> {

    List<Sheet> findAll();
}
