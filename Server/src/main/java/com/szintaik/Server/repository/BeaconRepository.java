package com.szintaik.Server.repository;

import com.szintaik.Server.model.Beacon;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BeaconRepository extends JpaRepository<Beacon, Long> {

    Beacon save (Beacon beacon);
}
