package com.szintaik.Server.repository;

import com.szintaik.Server.model.Value;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ValueRepository extends JpaRepository<Value, Long> {

    public void deleteById(Long id);
}
