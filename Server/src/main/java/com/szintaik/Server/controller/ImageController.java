package com.szintaik.Server.controller;




import com.google.common.io.Files;
import com.szintaik.Server.model.Image;
import com.szintaik.Server.service.ImageService;
import org.apache.commons.io.IOUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


import java.io.File;

import java.io.IOException;


import java.net.URISyntaxException;
import java.nio.file.Paths;


@RestController
@RequestMapping("/api/v1")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ImageController {

    private final ImageService imageService;

    public ImageController(ImageService imageService) {
        this.imageService = imageService;
    }

    @PostMapping("/image")
    public ResponseEntity<Image> imageUpload(@RequestParam("imageFile") MultipartFile image) throws IOException, URISyntaxException {


        return ResponseEntity.ok().body(imageService.upload(image));
    }

    @GetMapping(value = "/imageWithByte/{image_id}")
    public ResponseEntity<Image> getImage(@PathVariable("image_id") Long imageId) throws IOException {

        Image image = imageService.findById(imageId);
        image.setPicByte(java.nio.file.Files.readAllBytes(Paths.get("yoda.jpg")));
        byte[] array = java.nio.file.Files.readAllBytes(Paths.get("yoda.jpg"));
        System.out.println(array);


        image.setType("jpeg");
        return ResponseEntity.ok().body(imageService.findById(imageId));

    }

    @GetMapping(value = "/image/{image_id}")
    public ResponseEntity<Image> getImage1(@PathVariable("image_id") Long imageId) throws IOException {

        return ResponseEntity.ok().body(imageService.findById(imageId));
    }
}
