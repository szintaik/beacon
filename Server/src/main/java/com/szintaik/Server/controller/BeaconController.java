package com.szintaik.Server.controller;

import com.szintaik.Server.model.Beacon;
import com.szintaik.Server.model.Event;
import com.szintaik.Server.service.BeaconService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class BeaconController {

    private final BeaconService beaconService;

    public BeaconController(BeaconService beaconService) {
        this.beaconService = beaconService;
    }


    @PutMapping("/beacons")
    public ResponseEntity<Beacon> updateBeacon(@Valid @RequestBody Beacon beacon) {

        return ResponseEntity.ok().body(beaconService.update(beacon));
    }

    @GetMapping("/beacons/{id}")
    public ResponseEntity<Beacon> getBeacon(@PathVariable(value= "id") Long id){
        return ResponseEntity.ok().body(beaconService.findById(id));
    }


}
