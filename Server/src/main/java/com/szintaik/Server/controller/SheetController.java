package com.szintaik.Server.controller;

import com.szintaik.Server.model.Sheet;
import com.szintaik.Server.service.SheetService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class SheetController {

    private final SheetService sheetService;

    public SheetController(SheetService sheetService) {
        this.sheetService = sheetService;
    }

    @GetMapping("/sheets")
    public ResponseEntity<List<Sheet>> getAllSheets(){

        return ResponseEntity.ok().body(sheetService.findAll());
    }

    @GetMapping("/sheets/{id}")
    public ResponseEntity<Sheet> getSheet(@PathVariable(value= "id") Long id){

        return ResponseEntity.ok().body(sheetService.findById(id));
    }

    @PostMapping("/sheets")
    public ResponseEntity<Sheet> createSheet(@Valid @RequestBody Sheet sheet){
        return ResponseEntity.ok().body(sheetService.save(sheet));
    }

    @PutMapping("/sheets/{id}")
    public ResponseEntity<Sheet> updateSheet(@PathVariable(value= "id") Long id, @Valid @RequestBody Sheet sheetToUpdate){


        return ResponseEntity.ok().body(sheetService.update(sheetToUpdate));
    }


}
