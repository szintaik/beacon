package com.szintaik.Server.controller;

import com.szintaik.Server.model.Event;
import com.szintaik.Server.service.EventService;
import com.szintaik.Server.service.ImageService;
import com.szintaik.Server.service.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class EventController {

    private final EventService eventService;
    private final ImageService imageService;
    private final UserService userService;

    public EventController(EventService eventService, ImageService imageService, UserService userService) {
        this.eventService = eventService;
        this.imageService = imageService;
        this.userService = userService;
    }

    @PostMapping("/events")
    public ResponseEntity<Event> createEvent(@Valid @RequestBody Event event) throws IOException {
        return ResponseEntity.ok().body(eventService.save(event));
    }

    @PostMapping("/events/addbeacons")
    public ResponseEntity<Event> addBeaconsToEvent(@Valid @RequestBody Event event) throws IOException {
        return ResponseEntity.ok().body(eventService.addBeacons(event));
    }

    @GetMapping("/events")
    public ResponseEntity<List<Event>> getEvents(){
        return ResponseEntity.ok().body(eventService.findEvents());
    }

    @GetMapping("/events/public")
    public ResponseEntity<List<Event>> getPublicEvents(){
        return ResponseEntity.ok().body(eventService.findAll());
    }

    @DeleteMapping("/events/{id}")
    public ResponseEntity<Long> deleteEvent(@PathVariable(value= "id") Long id) throws IOException {

          eventService.delete(id);
        return ResponseEntity.ok().body(id);
    }




}
