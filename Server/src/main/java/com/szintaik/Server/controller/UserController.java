package com.szintaik.Server.controller;

import com.szintaik.Server.model.User;
import com.szintaik.Server.service.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(value ="/createrole")
    public void createRole() {
        userService.createRole();
    }

    @PostMapping(value = "/user/registration")
    public ResponseEntity<User> createUser(@RequestBody User user) {

        return ResponseEntity.ok().body(userService.save(user));
    }

    @GetMapping(value = "/user/profile")
    public ResponseEntity<User> userProfile() {
        return ResponseEntity.ok().body(userService.findActualUser());
    }

    @PutMapping(value = "/user")
    public ResponseEntity<User> updateUser(@RequestBody User user, @RequestParam(value = "password", required = false,
            defaultValue = "false") boolean password) {

        return ResponseEntity.ok().body(userService.update(user, password));
    }

    @PutMapping(value = "/user/updateUserToAdmin")
    public ResponseEntity<User> updateUserToAdmin(@RequestBody User user) {

        return ResponseEntity.ok().body(userService.updateUserToAdmin(user));
    }

}
