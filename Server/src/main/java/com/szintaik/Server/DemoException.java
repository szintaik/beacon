package com.szintaik.Server;

import org.springframework.http.HttpStatus;

public class DemoException extends  RuntimeException{

    private HttpStatus httpStatus;

    public DemoException(String message, HttpStatus httpStatus){
        super(message);
        this.httpStatus=httpStatus;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }
}
