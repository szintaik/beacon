package com.szintaik.Server.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
@Table( name = "events")
public class Event extends BaseEntity {


    private String name;

    private String place;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    @JoinColumn(name = "image_id")
    private Image floorMap;

    private String logoSrc;

    private String beaconsApiKey;

    @ManyToOne
    @JoinColumn(name = "user_id")
    @JsonIgnore
    private User user;

    @OneToMany(mappedBy = "event", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Beacon> beacons;

    private String beaconTypes;

    public Event() {
    }

    public Event(String name, String place, User user) {
        this.name = name;
        this.place = place;
        this.user = user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public Image getFloorMap() {
        return floorMap;
    }

    public void setFloorMap(Image floorMap) {
        this.floorMap = floorMap;
    }

    public String getLogoSrc() {
        return logoSrc;
    }

    public void setLogoSrc(String logoSrc) {
        this.logoSrc = logoSrc;
    }

    public String getBeaconsApiKey() {
        return beaconsApiKey;
    }

    public void setBeaconsApiKey(String beaconsApiKey) {
        this.beaconsApiKey = beaconsApiKey;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getBeaconTypes() {
        return beaconTypes;
    }

    public void setBeaconTypes(String beaconTypes) {
        this.beaconTypes = beaconTypes;
    }

    public List<Beacon> getBeacons() {
        return beacons;
    }

    public void setBeacons(List<Beacon> beacons) {
        this.beacons = beacons;
    }
}
