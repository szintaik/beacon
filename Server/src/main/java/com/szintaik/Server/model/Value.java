package com.szintaik.Server.model;



import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;

@Entity
@Table( name = "values")
public class Value extends BaseEntity{


    private String value;

    @ManyToOne
    @JoinColumn(name = "column_id")
    @JsonIgnore
    private Column column;

    public Value() {
    }

    public Value(String value, Column column) {
        this.value = value;
        this.column = column;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }


    public Column getColumn() {
        return column;
    }

    public void setColumn(Column column) {
        this.column = column;
    }
}
