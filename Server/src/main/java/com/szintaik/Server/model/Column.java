package com.szintaik.Server.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
@Table( name = "columns")
public class Column extends BaseEntity {

    private String name;

    @Enumerated(EnumType.STRING)
    private ColumnType type;

    @ManyToOne
    @JoinColumn(name = "sheet_id")
    @JsonIgnore
    private Sheet sheet;

    @OneToMany(mappedBy = "column", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Value> values;

    public Column() {
    }

    public Column(String name, ColumnType type, Sheet sheet) {
        this.name = name;
        this.type = type;
        this.sheet = sheet;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Value> getValues() {
        return values;
    }

    public void setValues(List<Value> values) {
        this.values = values;
    }

    public Sheet getSheet() {
        return sheet;
    }

    public void setSheet(Sheet sheet) {
        this.sheet = sheet;
    }

    public ColumnType getType() {
        return type;
    }

    public void setType(ColumnType type) {
        this.type = type;
    }
}
