package com.szintaik.Server.model;


import javax.persistence.*;
import java.util.List;

@Entity
@Table( name = "sheets")
public class Sheet extends BaseEntity {

    @OneToMany(mappedBy = "sheet", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Column> columns;

    @Transient
    private String src;

    public Sheet() {
    }

    public List<Column> getColumns() {
        return columns;
    }

    public void setColumns(List<Column> columns) {
        this.columns = columns;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }
}
