package com.szintaik.Server.service;

import com.szintaik.Server.model.Sheet;

import java.util.List;

public interface SheetService {

    Sheet save(Sheet sheet);

    Sheet findById(Long id);

    List<Sheet> findAll();

    Sheet update(Sheet sheet);



}
