package com.szintaik.Server.service;

import com.szintaik.Server.model.Event;
import com.szintaik.Server.model.User;

import java.io.IOException;
import java.util.List;

public interface EventService {

    public Event save(Event event) throws IOException;

    public Event addBeacons(Event event) throws IOException;

    public Event findById(Long id) throws IOException;

    public Event findByName(String name);

    public List<Event> findAll();

    public void delete(Long id) throws IOException;

    public List<Event> getPersonalEvents(User user);

    public List<Event> findEvents();
}
