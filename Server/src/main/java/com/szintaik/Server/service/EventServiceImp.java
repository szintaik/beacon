package com.szintaik.Server.service;


import com.google.common.io.Files;
import com.szintaik.Server.model.*;
import com.szintaik.Server.repository.*;
import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class EventServiceImp implements EventService {

    private final EventRepository eventRepository;
    private final BeaconRepository beaconRepository;
    private final UserService userService;
    private final ImageService imageService;
    private final ImageRepository imageRepository;
    private final SheetService sheetService;
    private final UserRepository userRepository;
    private final ValueRepository valueRepository;
    private final ColumnRepository columnRepository;
    private final SheetRepository sheetRepository;

    private final String ROLE_USER = "ROLE_USER";

    private static final String PATH = "D:\\data\\";

    public EventServiceImp(EventRepository eventRepository, BeaconRepository beaconRepository, com.szintaik.Server.service.UserService userService, ImageService imageService, ImageRepository imageRepository, SheetService sheetService, UserRepository userRepository, ValueRepository valueRepository, ColumnRepository columnRepository, SheetRepository sheetRepository) {
        this.eventRepository = eventRepository;
        this.beaconRepository = beaconRepository;
        this.userService = userService;
        this.imageService = imageService;
        this.imageRepository = imageRepository;
        this.sheetService = sheetService;
        this.userRepository = userRepository;
        this.valueRepository = valueRepository;
        this.columnRepository = columnRepository;
        this.sheetRepository = sheetRepository;
    }

    @Override
    public Event save(Event event) throws IOException {

        event = saveLogo(event);

        User user = userService.findActualUser();
        Image image = (imageService.findById(event.getFloorMap().getId()));

        image.setWidth(event.getFloorMap().getWidth());
        image.setHeight(event.getFloorMap().getHeight());
        image = imageRepository.save(image);
        event.setUser(user);
        event.setFloorMap(image);
        user.getEvents().add(event);


        eventRepository.save(event);
        userRepository.save(user);


        return event;
    }


    @Override
    public Event addBeacons(Event event) {

        for (Beacon beacon : event.getBeacons()) {
            beacon.setEvent(event);
        }
        User user = userService.findActualUser();
        event.setUser(user);

        return eventRepository.save(event);
    }

    @Override
    public Event findById(Long id) throws IOException {

        Event event = distinctEvent(eventRepository.findById(id).orElse(new Event()));
        //  event = setBase64Src(event);

        return event;
    }

    @Override
    public Event findByName(String name) {
        return eventRepository.findByName(name);
    }

    @Override
    public List<Event> findAll() {

        List<Event> events = eventRepository.findAll();

        events.forEach(event -> {

            try {
                event = distinctEvent(event);
                event = setBase64Src(event);
                event = setLogoBase64Src(event);
            } catch (IOException e) {
                e.printStackTrace();
            }


        });

        return events;
    }

    @Override
    public void delete(Long id) throws IOException {


        Event event = findById(id);
        User user = userService.findActualUser();
        deleteImagesDirectory(event);
        event.setFloorMap(null);

        user.getEvents().removeIf(e -> (e.getId() == event.getId()));

        if (event.getBeacons() != null) {

            event.getBeacons().forEach(beacon -> {
                if (beacon.getSheet() != null) {
                    if (beacon.getSheet().getColumns() != null) {
                        beacon.getSheet().getColumns().forEach(column -> {
                            if (column.getValues() != null) {
                                column.getValues().forEach(value -> {
                                    value.setColumn(null);
                                });
                            }
                            column.setValues(null);
                            column.setSheet(null);
                        });
                        beacon.getSheet().setColumns(null);
                    }
                }

                beacon.setEvent(null);
            });
        }
        event.setBeacons(null);

        valueRepository.findAll().forEach(value -> {
            if (value.getColumn() == null)
                valueRepository.delete(value);
        });

        columnRepository.findAll().forEach(column -> {
            if (column.getSheet() == null)
                columnRepository.delete(column);
        });


        beaconRepository.findAll().forEach(beacon -> {
            if (beacon.getEvent() == null) {
                if (beacon.getSheet() != null) {
                    sheetRepository.delete(beacon.getSheet());
                }
                beaconRepository.delete(beacon);
            }
        });


        eventRepository.delete(event);
        userRepository.save(user);

    }

    private void deleteImagesDirectory(Event event) throws IOException {

        String pathHelper = event.getLogoSrc();

        String[] pathHelperArray = pathHelper.split("\\\\");
        String path = "";

        for (int i = 0; i < pathHelperArray.length - 1; i++) {

            if (i > 0) {
                path += "\\" + pathHelperArray[i];
            }
            else {
                path += pathHelperArray[i];
            }
        }

        FileUtils.deleteDirectory(new File(path));

    }

    @Override
    public List<Event> getPersonalEvents(User user) {

        List<Event> events = user.getEvents();
        events.forEach(event -> {

            try {
                event = distinctEvent(event);
                event = setBase64Src(event);
                event = setLogoBase64Src(event);
            } catch (IOException e) {
                e.printStackTrace();
            }


        });

        return events;
    }

    @Override
    public List<Event> findEvents() {

        User user = userService.findActualUser();

        if (user.getRole().getRole().equals(ROLE_USER)) {
            return getPersonalEvents(user);
        } else {
            return findAll();
        }
    }

    private Event distinctEvent(Event event) {

        if (event.getBeacons() != null) {
            event.setBeacons(event.getBeacons().stream().distinct().collect(Collectors.toList()));
            event.getBeacons().forEach(beacon -> {
                if (beacon.getSheet() != null) {
                    Sheet sheet = beacon.getSheet();
                    List<Column> columns = sheet.getColumns().stream().distinct().collect(Collectors.toList());
                    sheet.setColumns(columns);
                    beacon.setSheet(sheet);
                }
            });
        }

        return event;
    }

    private Event setBase64Src(Event event) throws IOException {

        //floormap
        if (event.getFloorMap().getSrc().contains(PATH)) {

            if (new File(event.getFloorMap().getSrc()).exists()) {
                byte[] byteArray = java.nio.file.Files.readAllBytes(Paths.get(event.getFloorMap().getSrc()));
                event.getFloorMap().setPicBase64(Base64.getEncoder().encodeToString(byteArray));
            }

        }
        //values

        if (event.getBeacons() != null) {
            event.getBeacons().forEach(beacon -> {
                if (beacon.getSheet() != null && beacon.getSheet().getColumns() != null) {
                    beacon.getSheet().getColumns().forEach(column -> {
                        if (column.getType() == ColumnType.IMAGE) {
                            column.getValues().forEach(value -> {
                                try {
                                    if (value.getValue().contains(PATH)) {
                                        byte[] valueImageByteArray = java.nio.file.Files.readAllBytes(Paths.get(value.getValue()));
                                        value.setValue(Base64.getEncoder().encodeToString(valueImageByteArray));
                                    }
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }

                            });
                        }
                    });
                }
            });
        }

        return event;
    }


    private Event saveLogo(Event event) throws IOException {

        String extensionHelper = event.getLogoSrc().split(";")[0];
        String extension = extensionHelper.split("/")[1];

        String base64Src = event.getLogoSrc().split(",")[1];
        byte[] src = Base64.getDecoder().decode(base64Src.getBytes());
        File file = new File(PATH + event.getFloorMap().getId() + "_images//" + event.getName() + "." + extension);
        Files.write(src, file);

        event.setLogoSrc(file.getPath());


        return event;
    }

    private Event setLogoBase64Src(Event event) throws IOException {

        if (event.getLogoSrc() != null) {

            byte[] logoByteArray = java.nio.file.Files.readAllBytes(Paths.get(event.getLogoSrc()));
            event.setLogoSrc(Base64.getEncoder().encodeToString(logoByteArray));

        }
        return event;
    }

}
