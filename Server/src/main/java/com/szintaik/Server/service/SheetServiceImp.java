package com.szintaik.Server.service;

import com.google.common.io.Files;
import com.szintaik.Server.model.Column;
import com.szintaik.Server.model.ColumnType;
import com.szintaik.Server.model.Sheet;
import com.szintaik.Server.model.Value;
import com.szintaik.Server.repository.BeaconRepository;
import com.szintaik.Server.repository.ColumnRepository;
import com.szintaik.Server.repository.SheetRepository;
import com.szintaik.Server.repository.ValueRepository;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SheetServiceImp implements SheetService {

    private final SheetRepository sheetRepository;
    private final ValueRepository valueRepository;
    private final ColumnRepository columnRepository;
    private final BeaconRepository beaconRepository;
    private int columnPosition = 0;

    private static final String PATH= "D:\\data\\";

    public SheetServiceImp(SheetRepository sheetRepository, ValueRepository valueRepository, ColumnRepository columnRepository, BeaconRepository beaconRepository) {
        this.sheetRepository = sheetRepository;
        this.valueRepository = valueRepository;
        this.columnRepository = columnRepository;
        this.beaconRepository = beaconRepository;
    }

    @Override
    public Sheet save(Sheet sheet) {


        for (Column column : sheet.getColumns()) {
            column.setSheet(sheet);

            if (column.getType() == ColumnType.IMAGE) {

                column = columnBase64ToSrc(column, columnPosition,sheet);
            }
            for (Value value : column.getValues()) {
                value.setColumn(column);
            }
            columnPosition++;
        }

        return  sheetRepository.save(sheet);
    }


    @Override
    public Sheet update(Sheet sheet) {

        Sheet sheetToUpdate = findById(sheet.getId());

        sheetToUpdate.getColumns().get(0).setValues(null);

        sheetRepository.save(sheetToUpdate);
        return sheet;
    }

    @Override
    public Sheet findById(Long id) {

        Sheet sheet = sheetRepository.findById(id).orElse(new Sheet());
        List<Column> columns = sheet.getColumns().stream().distinct().collect(Collectors.toList());
        sheet.setColumns(columns);

        return sheet;
    }

    @Override
    public List<Sheet> findAll() {
        return sheetRepository.findAll();
    }


    private Column columnBase64ToSrc(Column column,int columnPosition, Sheet sheet) {

        File file = null;

        String columName = column.getName();


        for (int i = 0; i < column.getValues().size(); i++) {
            try {
                String extensionHelper = column.getValues().get(i).getValue().split(";")[0];
                String extension = extensionHelper.split("/")[1];

                String base64Src = column.getValues().get(i).getValue().split(",")[1];
                byte[] src = Base64.getDecoder().decode(base64Src.getBytes());
                file = new File(PATH+ sheet.getSrc() + "\\" + i + "_" + columnPosition +"_" + columName + "." + extension);
                Files.write(src, file);

                column.getValues().get(i).setValue(file.getPath());

            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        return column;

    }
}
