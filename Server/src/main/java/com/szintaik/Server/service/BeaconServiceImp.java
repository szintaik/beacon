package com.szintaik.Server.service;

import com.szintaik.Server.model.Beacon;
import com.szintaik.Server.repository.BeaconRepository;
import org.springframework.stereotype.Service;

@Service
public class BeaconServiceImp implements BeaconService{

    private final BeaconRepository beaconRepository;

    private final SheetService sheetService;

    private final EventService eventService;

    public BeaconServiceImp(BeaconRepository beaconRepository, SheetService sheetService, EventService eventService) {
        this.beaconRepository = beaconRepository;
        this.sheetService = sheetService;
        this.eventService = eventService;
    }

    @Override
    public Beacon save(Beacon beacon) {

        return beaconRepository.save(beacon);
    }

    @Override
    public Beacon findById(Long id) {
        return beaconRepository.findById(id).orElse(new Beacon());
    }

    @Override
    public Beacon update(Beacon beacon) {
        beacon.setEvent(findById(beacon.getId()).getEvent());
        beacon.setSheet(sheetService.findById(beacon.getSheet().getId()));

        return beaconRepository.save(beacon);
    }
}
