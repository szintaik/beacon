package com.szintaik.Server.service;

import com.google.common.io.Files;
import com.szintaik.Server.model.Image;
import com.szintaik.Server.model.User;
import com.szintaik.Server.repository.ImageRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

@Service
public class ImageServiceImp implements ImageService {

    private final ImageRepository imageRepository;

    private final UserService userService;

    private static final String PATH = "D:\\data\\";

    public ImageServiceImp(ImageRepository imageRepository, UserService userService) {
        this.imageRepository = imageRepository;
        this.userService = userService;
    }

    @Override
    public Image upload(MultipartFile multipartFile) throws IOException {

        User user = userService.findActualUser();

        Image image = new Image();
        image.setName(multipartFile.getOriginalFilename());
        image.setType(Files.getFileExtension(multipartFile.getOriginalFilename()));

        image = imageRepository.save(image);

        String UNIQUE_PATH = PATH +image.getId() + "_images\\";

        File filePath = new File(UNIQUE_PATH);

        if (!filePath.exists()) {
            filePath.mkdirs();
        }

        File file = new File(UNIQUE_PATH + image.getId() +"_" +multipartFile.getOriginalFilename());


        Files.write(multipartFile.getBytes(), file);

        image.setSrc(file.getPath());

        image = imageRepository.save(image);


        return image;
    }

    @Override
    public Image save(Image image) {
        return imageRepository.save(image);
    }


    @Override
    public Image findById(Long id) throws IOException {
        Image image = imageRepository.findById(id).orElse(new Image());
        image.setPicByte(java.nio.file.Files.readAllBytes(Paths.get(image.getSrc())));

        return image;
    }

    @Override
    public void delete(Image image) {
        imageRepository.delete(image);
    }
}
