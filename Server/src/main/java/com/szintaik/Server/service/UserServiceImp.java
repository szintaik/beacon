package com.szintaik.Server.service;

import com.szintaik.Server.model.Role;
import com.szintaik.Server.model.User;
import com.szintaik.Server.repository.RoleRepository;
import com.szintaik.Server.repository.UserRepository;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;


@Service
public class UserServiceImp implements UserService {

    private final UserRepository userRepository;

    private final RoleRepository roleRepository;

    private final String ROLE_USER = "ROLE_USER";

    private final String ROLE_ADMIN = "ROLE_ADMIN";

    public UserServiceImp(UserRepository userRepository, RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
    }

    @Override
    public User save(User user) {

        user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
        user.setRole(roleRepository.findByRole(ROLE_USER));
        return userRepository.save(user);
    }

    @Override
    public User findActualUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return userRepository.findByEmail(auth.getName());

    }

    @Override
    public User update(User user, boolean password) {

        User userToUpdate = findActualUser();

        userToUpdate.setEmail(user.getEmail());
        userToUpdate.setFullName(user.getFullName());

        if (password) {
            userToUpdate.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
        }


        return userRepository.save(userToUpdate);

    }

    @Override
    public void createRole() {

        Role roleUser = new Role();
        roleUser.setRole("ROLE_USER");
        roleRepository.save(roleUser);

        Role roleAdmin = new Role();
        roleAdmin.setRole("ROLE_ADMIN");
        roleRepository.save(roleAdmin);

    }

    @Override
    public User updateUserToAdmin(User user) {

        user.setRole(roleRepository.findByRole(ROLE_ADMIN));

        return userRepository.save(user);
    }
}
