package com.szintaik.Server.service;

import com.szintaik.Server.model.Beacon;

public interface BeaconService {

    public Beacon save(Beacon beacon);

    public Beacon findById(Long id);

    public Beacon update(Beacon beacon);
}
