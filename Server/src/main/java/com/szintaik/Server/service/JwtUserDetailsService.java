package com.szintaik.Server.service;

import com.szintaik.Server.model.User;
import com.szintaik.Server.repository.UserRepository;
import com.szintaik.Server.util.UserPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

@Service
public class JwtUserDetailsService implements UserDetailsService {

    private final UserRepository repository;

    public JwtUserDetailsService(UserRepository repository) {
        this.repository = repository;
    }

    public UserDetails loadUserByUsername(String username){
        User user = repository.findByEmail(username);

            return new UserPrincipal(user);
    }
}
