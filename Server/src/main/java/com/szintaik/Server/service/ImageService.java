package com.szintaik.Server.service;

import com.szintaik.Server.model.Image;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.URISyntaxException;

public interface ImageService {

    public Image save(Image image);

    public Image upload(MultipartFile file) throws IOException, URISyntaxException;

    public Image findById(Long id) throws IOException;

    public void delete(Image image);
}
