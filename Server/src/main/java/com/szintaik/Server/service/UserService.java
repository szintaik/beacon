package com.szintaik.Server.service;

import com.szintaik.Server.model.User;

public interface UserService {

    public User save(User user);

    public User findActualUser();

    public User update(User user, boolean password);

    public void createRole();

    public User updateUserToAdmin(User user);

}
