package com.example.beacon_android.service;

import com.example.beacon_android.constant.Constant;
import com.example.beacon_android.model.Event;

import java.io.UnsupportedEncodingException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class EventServiceImp {

    private Retrofit retrofit;

    private EventService eventService;
    private List<Event> events = null;
    private Event event = null;

    private GenericCallback callback = null;


    public EventServiceImp(GenericCallback callback) {
        retrofit = new Retrofit.Builder()
                .baseUrl(Constant.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        eventService = retrofit.create(EventService.class);
        this.callback = callback;
    }


    public void getPublicEvents() {

        Call<List<Event>> getPublicEvents = eventService.getPublicEvents();

        getPublicEvents.enqueue(new Callback<List<Event>>() {
            @Override
            public void onResponse(Call<List<Event>> call, Response<List<Event>> response) {
                if (response.isSuccessful()) {
                    events = response.body();

                        callback.success(events);


                } else {

                }
            }

            @Override
            public void onFailure(Call<List<Event>> call, Throwable t) {

            }
        });

    }

    public void getMyEvents(String token){

        Call<List<Event>> getMyEvents = eventService.getMyEvents(token);

        getMyEvents.enqueue(new Callback<List<Event>>() {
            @Override
            public void onResponse(Call<List<Event>> call, Response<List<Event>> response) {

                if (response.isSuccessful()) {
                    events = response.body();

                    callback.success(events);


                } else {

                }

            }

            @Override
            public void onFailure(Call<List<Event>> call, Throwable t) {

            }
        });

    }

}
