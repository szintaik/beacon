package com.example.beacon_android.service;

import android.Manifest;
import android.content.Context;
import android.os.Build;


import androidx.annotation.RequiresApi;

import com.example.beacon_android.model.Beacon;
import com.example.beacon_android.model.Event;
import com.kontakt.sdk.android.ble.connection.OnServiceReadyListener;
import com.kontakt.sdk.android.ble.manager.ProximityManager;
import com.kontakt.sdk.android.ble.manager.ProximityManagerFactory;
import com.kontakt.sdk.android.ble.manager.listeners.EddystoneListener;
import com.kontakt.sdk.android.ble.manager.listeners.IBeaconListener;
import com.kontakt.sdk.android.ble.manager.listeners.simple.SimpleEddystoneListener;
import com.kontakt.sdk.android.ble.manager.listeners.simple.SimpleIBeaconListener;
import com.kontakt.sdk.android.common.KontaktSDK;
import com.kontakt.sdk.android.common.profile.IBeaconDevice;
import com.kontakt.sdk.android.common.profile.IBeaconRegion;
import com.kontakt.sdk.android.common.profile.IEddystoneDevice;
import com.kontakt.sdk.android.common.profile.IEddystoneNamespace;

import java.util.List;


public class BeaconListener {

   // private static final String API_KEY = "cRBBPqUAjtRzlmsWxlVYaSyukXUKNrxl";
    private final String API_KEY;

    private static final String[] INITIAL_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION
    };

    private Event event;

    private ProximityManager proximityManager;

    private Context activity;

    private BeaconListenerCallback beaconListenerCallback = null;

    private Beacon closestBeacon = null;

    private int closestBeaconRssi = 0;

    public BeaconListener(String api_key, Context activity, Event event, BeaconListenerCallback beaconListenerCallback) {
        API_KEY = api_key;

        this.event = event;

        KontaktSDK.initialize(API_KEY);

        this.activity = activity;
        this.beaconListenerCallback = beaconListenerCallback;
        proximityManager = ProximityManagerFactory.create(this.activity);
        proximityManager.setIBeaconListener(createIBeaconListener());
        proximityManager.setEddystoneListener(createEddystoneListener());    }

    private EddystoneListener createEddystoneListener() {
        return new SimpleEddystoneListener() {
            @Override
            public void onEddystoneDiscovered(IEddystoneDevice eddystone, IEddystoneNamespace namespace) {

            }
        };
    }



    private IBeaconListener createIBeaconListener() {
        return new SimpleIBeaconListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onIBeaconDiscovered(final IBeaconDevice ibeacon, IBeaconRegion region) {
                System.out.println("!!!!!!!!!!!!!!!!!!!");
                System.out.println(ibeacon.getUniqueId());
            }

            @Override
            public void onIBeaconsUpdated(List<IBeaconDevice> iBeacons, IBeaconRegion region) {

                for (IBeaconDevice b : iBeacons) {
                    System.out.println("updated");
                    System.out.println(b.getUniqueId());
                        if(closestBeacon == null || closestBeaconRssi < b.getRssi()){
                            closestBeaconRssi = b.getRssi();
                            for(Beacon beacon : event.getBeacons()){
                                if(beacon.getUniqueId().equals(b.getUniqueId())){
                                    closestBeacon = beacon;

                                }
                            }
                        }
                }

                beaconListenerCallback.beaconFound(closestBeacon);
            }

            @Override
            public void onIBeaconLost(IBeaconDevice iBeacon, IBeaconRegion region) {

            }
        };
    }





    public void startScanning() {
        closestBeacon = null;

        closestBeaconRssi = 0;
        proximityManager.connect(new OnServiceReadyListener() {
            @Override
            public void onServiceReady() {
                proximityManager.startScanning();
            }
        });
    }

    public void stopScanning(){
        proximityManager.stopScanning();
    }

    public void disconnect(){
        proximityManager.disconnect();
    }





}
