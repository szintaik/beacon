package com.example.beacon_android.service;


public interface GenericCallback<T> {
   public void success(T result) ;
}
