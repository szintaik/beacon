package com.example.beacon_android.service;

import com.example.beacon_android.model.Event;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;

public interface EventService {

    @GET("events/public")
    Call<List<Event>> getPublicEvents();

    @GET("events")
    Call<List<Event>> getMyEvents(@Header("Authorization") String token);
}
