package com.example.beacon_android.model;

import java.util.List;

public class Event {

    private Long id;

    private String name;

    private String place;

    private String beaconsApiKey;

    private Image floorMap;

    private String logoSrc;

    private String beaconTypes;

    private List<Beacon> beacons;

    public Event() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public Image getFloorMap() {
        return floorMap;
    }

    public void setFloorMap(Image floorMap) {
        this.floorMap = floorMap;
    }

    public String getLogoSrc() {
        return logoSrc;
    }

    public void setLogoSrc(String logoSrc) {
        this.logoSrc = logoSrc;
    }

    public String getBeaconTypes() {
        return beaconTypes;
    }

    public void setBeaconTypes(String beaconTypes) {
        this.beaconTypes = beaconTypes;
    }

    public List<Beacon> getBeacons() {
        return beacons;
    }

    public void setBeacons(List<Beacon> beacons) {
        this.beacons = beacons;
    }

    public String getBeaconsApiKey() {
        return beaconsApiKey;
    }

    public void setBeaconsApiKey(String beaconsApiKey) {
        this.beaconsApiKey = beaconsApiKey;
    }

    @Override
    public String toString() {
        return name;
    }
}
