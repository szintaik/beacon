package com.example.beacon_android.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import com.example.beacon_android.R;
import com.example.beacon_android.model.JwtRequest;
import com.example.beacon_android.model.JwtResponse;
import com.example.beacon_android.service.GenericCallback;
import com.example.beacon_android.service.UserServiceImp;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;

public class LoginActivity extends AppCompatActivity implements Serializable, GenericCallback {

    UserServiceImp userServiceImp;

    JwtResponse jwtResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        findViewById(R.id.btn_login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }

        });
        findViewById(R.id.btn_signUpHere).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               signUp();
            }

        });
    }




    private void login() {

        userServiceImp = new UserServiceImp(this);

        JwtRequest jwtRequest = new JwtRequest();

        EditText email = (EditText) findViewById(R.id.txt_email);
        EditText password = (EditText) findViewById(R.id.txt_password);

        jwtRequest.setUsername(email.getText().toString());
        jwtRequest.setPassword(password.getText().toString());

        userServiceImp.login(jwtRequest);


    }

    private void signUp() {
        Intent intentLoginToRegistration = new Intent(LoginActivity.this, RegistrationActivity.class);

        startActivity(intentLoginToRegistration);
    }

    @Override
    public void success(Object token)  {
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("MyPref", 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString("token", "Bearer " + token);
        editor.commit();

        Intent intentLoginToListEvents = new Intent(LoginActivity.this, ListEventsActivity.class);

        startActivity(intentLoginToListEvents);

    }



}
