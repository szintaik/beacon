package com.example.beacon_android.service.recycleview;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.beacon_android.R;
import com.example.beacon_android.model.Beacon;
import com.example.beacon_android.model.Row;

import java.util.ArrayList;
import java.util.List;

public class SheetListAdapter extends RecyclerView.Adapter<SheetListAdapter.MyViewHolder> {

    private List<Row> rows;

    private RecycleViewCallback callback;


    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout lstTexts;
        public LinearLayout lstImages;
        public RelativeLayout relativeLayout;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.lstTexts = (LinearLayout) itemView.findViewById(R.id.lst_Texts);
            this.lstImages = (LinearLayout) itemView.findViewById(R.id.lst_Images);

        }
    }


    public SheetListAdapter(List<Row> rows, RecycleViewCallback callback) {
        this.rows = rows;
        this.callback = callback;
    }

    @NonNull
    @Override
    public SheetListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.sheet_list_item, parent, false);
        SheetListAdapter.MyViewHolder viewHolder = new MyViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull SheetListAdapter.MyViewHolder holder, int position) {
         final Row row = rows.get(position);

        for (final ImageView img: row.getImages()) {
            img.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    callback.onClickEvent(img);
                }
            });
            holder.lstImages.addView(img);
        }
        for (TextView txt: row.getTexts()) {
            holder.lstTexts.addView(txt);
        }

    }

    @Override
    public int getItemCount() {
        return rows.size();
    }
}
