package com.example.beacon_android.service;

import com.example.beacon_android.model.Beacon;

public interface BeaconListenerCallback {
    public void beaconFound(Beacon beacon);
}
