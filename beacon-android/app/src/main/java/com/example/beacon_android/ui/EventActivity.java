package com.example.beacon_android.ui;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.example.beacon_android.R;
import com.example.beacon_android.model.Beacon;
import com.example.beacon_android.service.recycleview.BeaconListAdapter;
import com.example.beacon_android.model.Event;
import com.example.beacon_android.service.BeaconListener;
import com.example.beacon_android.service.BeaconListenerCallback;
import com.example.beacon_android.service.EventServiceImp;
import com.example.beacon_android.service.recycleview.RecycleViewCallback;
import com.google.gson.Gson;

public class EventActivity extends AppCompatActivity implements BeaconListenerCallback, ActivityCompat.OnRequestPermissionsResultCallback, RecycleViewCallback {

    private Event event;
    private EventServiceImp eventServiceImp;
    private Long eventId;
    private ListView beaconsListView;

    private BeaconListener beaconListener = null;

    private Switch switchBeaconListener;
    private  RecyclerView recyclerViewBeacons;

    boolean permissionGranted;


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);

        getPermission();
        getEvent();
        setSwitchBeaconListener();
        toolbarClickEvents();
    }


    private void setBeaconListener() {

        if (permissionGranted) {
            if (beaconListener == null) {
                beaconListener = new BeaconListener(event.getBeaconsApiKey(), this, event, this);
            }
            beaconListener.startScanning();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 200);
        }

    }


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private void getEvent() {

        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("MyPref", 0);
        Gson gson = new Gson();
        String json = sharedPreferences.getString("event", "");
        event = gson.fromJson(json, Event.class);

        setMap();

        setRecycleView();

        TextView beaconTypes = (TextView) findViewById(R.id.txt_beaconTypes);
        beaconTypes.setText(event.getBeaconTypes());



    }

    private void setRecycleView() {

        recyclerViewBeacons = (RecyclerView) findViewById(R.id.recycleView_beacons);
        BeaconListAdapter adapter = new BeaconListAdapter(event.getBeacons(), this);
        recyclerViewBeacons.setHasFixedSize(true);
        recyclerViewBeacons.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewBeacons.setAdapter(adapter);
    }

    @SuppressLint("ResourceType")
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private void setMap() {

        RelativeLayout bg_floorMap = (RelativeLayout) findViewById(R.id.bg_floormap);
        event.getFloorMap().setPicByte(Base64.decode(event.getFloorMap().getPicBase64(), Base64.DEFAULT));
        Bitmap bitmap = BitmapFactory.decodeByteArray(event.getFloorMap().getPicByte(), 0, event.getFloorMap().getPicByte().length);


        BitmapDrawable bitmapDrawable = new BitmapDrawable(getResources(), bitmap);
        bg_floorMap.setBackground(bitmapDrawable);

        //screen size in pixels
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;

        bg_floorMap.getLayoutParams().height = (int) (((double) width * (double) event.getFloorMap().getHeight()) / (double) event.getFloorMap().getWidth());


        ImageView marker;
        RelativeLayout.LayoutParams params;


        double beaconCoordinateRate = ((double) width / (double) 300);

        for (Beacon beacon : event.getBeacons()) {
            marker = new ImageView(this);
            params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            params.width = 75;
            params.height = 120;
            params.leftMargin = (int)( (double)beacon.getxCoordinate() * beaconCoordinateRate - (double)(75 / 2));
            params.topMargin =(int)( (double)beacon.getyCoordinate() * beaconCoordinateRate - (double)100);
            marker.setImageResource(R.drawable.marker);
            bg_floorMap.addView(marker, params);
        }


    }


    @Override
    public void beaconFound(final Beacon beacon) {

        switchBeaconListener.setChecked(false);

       if(beacon != null){

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(EventActivity.this);


        alertDialogBuilder.setTitle("Beacon found");


        alertDialogBuilder
                .setMessage("Closest beacon: " + beacon.getName())
                .setCancelable(false)
                .setPositiveButton("Open", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        setBeaconToSharedPreferences(beacon);

                        Intent intentEventToSheet = new Intent(EventActivity.this, SheetActivity.class);

                        startActivity(intentEventToSheet);
                    }
                })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        dialog.cancel();
                    }
                });


        AlertDialog alertDialog = alertDialogBuilder.create();


        alertDialog.show();
       }

    }


    private void setSwitchBeaconListener() {

        switchBeaconListener = (Switch) findViewById(R.id.swtch_Scanbeacons);

        switchBeaconListener.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    setBeaconListener();
                } else {
                    beaconListener.stopScanning();
                    beaconListener.disconnect();
                }
            }
        });
    }

    private void getPermission() {
        permissionGranted = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 200: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    setBeaconListener();
                }
            }
        }
    }

    private void setBeaconToSharedPreferences(Beacon beacon) {

        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("MyPref", 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        Gson gson = new Gson();
        String json = gson.toJson(beacon);
        editor.putString("beacon", json);
        editor.commit();


    }

    private void toolbarClickEvents() {

        final ImageView btnLogout = (ImageView) findViewById(R.id.btn_logoutToolbar);
        final ImageView btnBack = (ImageView) findViewById(R.id.btn_backToolbar);

        btnLogout.setVisibility(View.INVISIBLE);
        ((ImageView) findViewById(R.id.btn_profileToolbar)).setVisibility(View.INVISIBLE);
        btnBack.setVisibility(View.VISIBLE);

        final Activity thisActivity = this;

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                thisActivity.finish();
            }
        });
    }

    @Override
    public void onClickEvent(Object result) {

        Beacon beacon = (Beacon)result;

        setBeaconToSharedPreferences(beacon);

        Intent intentEventToSheet = new Intent(EventActivity.this, SheetActivity.class);

        startActivity(intentEventToSheet);
    }
}
