package com.example.beacon_android.service.recycleview;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.beacon_android.R;
import com.example.beacon_android.model.Beacon;

import java.util.List;

public class BeaconListAdapter extends RecyclerView.Adapter<BeaconListAdapter.MyViewHolder> {

    private List<Beacon> beacons;

    private RecycleViewCallback callback;


    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView textView;
        public RelativeLayout relativeLayout;
        public MyViewHolder(View itemView) {
            super(itemView);
            this.textView = (TextView) itemView.findViewById(R.id.txtBeaconNameListItem);
            relativeLayout = (RelativeLayout)itemView.findViewById(R.id.lstBeaconsRelativeLayout);
        }
    }


    public BeaconListAdapter(List<Beacon> beacons, RecycleViewCallback callback) {
        this.beacons = beacons;
        this.callback = callback;
    }

    @NonNull
    @Override
    public BeaconListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.beacon_list_item, parent, false);
        BeaconListAdapter.MyViewHolder viewHolder = new MyViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull BeaconListAdapter.MyViewHolder holder, int position) {
        final Beacon beacon = beacons.get(position);
        holder.textView.setText(beacon.getName());
     //  holder.imageView.setImageResource(listdata[position].getImgId());
        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callback.onClickEvent(beacon);
            }
        });
    }

    @Override
    public int getItemCount() {
        return beacons.size();
    }
}
