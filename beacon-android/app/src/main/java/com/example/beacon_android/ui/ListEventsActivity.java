package com.example.beacon_android.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;

import com.example.beacon_android.R;
import com.example.beacon_android.model.Event;
import com.example.beacon_android.service.recycleview.EventListAdapter;
import com.example.beacon_android.service.EventServiceImp;
import com.example.beacon_android.service.GenericCallback;
import com.example.beacon_android.service.recycleview.RecycleViewCallback;
import com.google.gson.Gson;


import java.util.List;


public class ListEventsActivity extends AppCompatActivity implements GenericCallback, RecycleViewCallback {

    private EventServiceImp eventServiceImp;
    private List<Event> events;
    private ListView eventsListView;

    private RecyclerView recyclerViewEventList;

    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_events);


        toolbarClickEvents();
    }


    @Override
    protected void onStart() {
        super.onStart();
        getPublicEvents();
    }

    private void getPublicEvents() {
        eventServiceImp = new EventServiceImp(this);
        eventServiceImp.getPublicEvents();

    }


    @Override
    public void success(Object result) {

        events = (List<Event>) result;

        recyclerViewEventList = (RecyclerView) findViewById(R.id.recycleView_Events);
        EventListAdapter adapter = new EventListAdapter(events, this, this);
        recyclerViewEventList.setHasFixedSize(true);
        recyclerViewEventList.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewEventList.setAdapter(adapter);

    }

    @Override
    public void onClickEvent(Object result) {

        Event event = (Event) result;

        sharedPreferences = getApplicationContext().getSharedPreferences("MyPref", 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        Gson gson = new Gson();
        String json = gson.toJson(event);
        editor.putString("event", json);
        editor.commit();

        Intent intentListEventsToEvent = new Intent(ListEventsActivity.this, EventActivity.class);

        startActivity(intentListEventsToEvent);

    }


    private void toolbarClickEvents() {

        final ImageView btnLogout = (ImageView) findViewById(R.id.btn_logoutToolbar);

        if(isUserLoggedIn()){
        btnLogout.setVisibility(View.VISIBLE);}
        else{
            btnLogout.setVisibility(View.INVISIBLE);
        }

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sharedPreferences = getApplicationContext().getSharedPreferences("MyPref", 0);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.remove("token");
                editor.commit();
                btnLogout.setVisibility(View.INVISIBLE);
            }
        });

        findViewById(R.id.btn_profileToolbar).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isUserLoggedIn()) {
                    Intent intentHomeToLogin = new Intent(ListEventsActivity.this, LoginActivity.class);

                    startActivity(intentHomeToLogin);
                }
                else{
                    Intent intentHomeToProfile = new Intent(ListEventsActivity.this, ProfileActivity.class);

                    startActivity(intentHomeToProfile);
                }
            }
        });
    }


    private boolean isUserLoggedIn() {
        sharedPreferences = getApplicationContext().getSharedPreferences("MyPref", 0);
        String token = sharedPreferences.getString("token", null);

        if (token == null) {
            return false;
        } else {
            return true;
        }
    }

}
