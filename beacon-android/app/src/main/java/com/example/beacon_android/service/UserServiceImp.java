package com.example.beacon_android.service;

import android.util.Base64;
import android.util.Log;

import com.example.beacon_android.constant.Constant;
import com.example.beacon_android.model.JwtRequest;
import com.example.beacon_android.model.JwtResponse;
import com.example.beacon_android.model.User;

import java.io.UnsupportedEncodingException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class UserServiceImp {

    Retrofit retrofit;

    UserService userService;

    JwtRequest jwtRequest;

    private GenericCallback callback = null;


    public UserServiceImp(GenericCallback callback) {
        retrofit = new Retrofit.Builder()
                .baseUrl(Constant.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        userService = retrofit.create(UserService.class);
        this.callback = callback;
    }


    public void login(JwtRequest jwtRequest) {

        Call<JwtResponse> loginUser = userService.login(jwtRequest);

        loginUser.enqueue(new Callback<JwtResponse>() {
            @Override
            public void onResponse(Call<JwtResponse> call, Response<JwtResponse> response) {

                if (response.isSuccessful()) {

                    try {
                        decoded(response.headers().get("Authorization"));
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    callback.success(response.headers().get("Authorization"));


                } else {

                }

            }

            @Override
            public void onFailure(Call<JwtResponse> call, Throwable t) {

            }
        });
    }


    public void registration(User user) {

        Call<User> registerUser = userService.registration(user);

        registerUser.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()) {

                    callback.success(response.body());

                } else {
                }

            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {

            }
        });


    }

    public void getUserProfile(String token) {


        Call<User> userProfile = userService.getUserProfile(token);

        userProfile.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()) {
                    callback.success(response.body());
                } else {
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {

            }
        });

    }

    public void updateUser(String token, User user){

        Call<User> updateUser = userService.updateUser(token, user);

        updateUser.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()) {
                    callback.success(response.body());
                } else {
                    System.out.println("else ág");
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                System.out.println("onfailure ág");
            }
        });

    }


    private void decoded(String token) throws UnsupportedEncodingException {

        String[] split = token.split("\\.");
        System.out.println("Header: " + getJson(split[0]));
        System.out.println("Body: " + getJson(split[1]));


    }

    private String getJson(String strEncoded) throws UnsupportedEncodingException {
        byte[] decodedBytes = Base64.decode(strEncoded, Base64.URL_SAFE);
        return new String(decodedBytes, "UTF-8");
    }


}
