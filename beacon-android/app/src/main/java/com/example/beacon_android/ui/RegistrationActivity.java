package com.example.beacon_android.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;

import com.example.beacon_android.R;
import com.example.beacon_android.model.User;
import com.example.beacon_android.service.GenericCallback;
import com.example.beacon_android.service.UserServiceImp;

public class RegistrationActivity extends AppCompatActivity implements GenericCallback {

    UserServiceImp userServiceImp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);


        findViewById(R.id.btn_registration).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registration();
            }
        });
    }


    private void registration(){

        userServiceImp = new UserServiceImp(this);

        User user = new User();

        EditText email = (EditText) findViewById(R.id.txt_emailSignUp);
        EditText fullName = (EditText) findViewById(R.id.txt_fullNameSignUp);
        EditText password = (EditText) findViewById(R.id.txt_passwordSignUp);

        user.setEmail(email.getText().toString());
        user.setFullName(fullName.getText().toString());
        user.setPassword(password.getText().toString());

        userServiceImp.registration(user);

    }

    @Override
    public void success(Object result) {

        Intent intentRegistrationToLogin = new Intent(RegistrationActivity.this, LoginActivity.class);

        startActivity(intentRegistrationToLogin);
    }
}
