package com.example.beacon_android.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.beacon_android.R;
import com.example.beacon_android.model.Event;
import com.example.beacon_android.service.EventServiceImp;
import com.example.beacon_android.service.GenericCallback;
import com.google.gson.Gson;

import java.util.List;

public class MyEventsActivity extends AppCompatActivity implements GenericCallback {

    private EventServiceImp eventServiceImp;
    private List<Event> events;
    private ListView eventsListView;

    private SharedPreferences sharedPreferences ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_events);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getSupportActionBar().hide();
    }

    @Override
    protected void onStart() {
        super.onStart();

        getMyEvents();
    }

    private void getMyEvents() {

        eventServiceImp = new EventServiceImp(this);
        eventServiceImp.getMyEvents(getToken());

    }


    @Override
    public void success(Object result) {

        events = (List<Event>)result;

        eventsListView = (ListView)findViewById(R.id.lstView_events);
        ArrayAdapter<Event> arrayAdapter = new ArrayAdapter<>(this, R.layout.activity_listview, R.id.textView, events);
        eventsListView.setAdapter(arrayAdapter);

        eventsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("MyPref", 0);
                SharedPreferences.Editor editor = sharedPreferences.edit();

                Gson gson = new Gson();
                String json = gson.toJson(events.get(position));
                editor.putString("event", json);
                editor.commit();

                Intent intentListEventsToEvent = new Intent(MyEventsActivity.this, EventActivity.class);

                startActivity(intentListEventsToEvent);
            }
        });

    }


    private String getToken(){
        sharedPreferences = getApplicationContext().getSharedPreferences("MyPref", 0);
        return sharedPreferences.getString("token", null);
    }
}
