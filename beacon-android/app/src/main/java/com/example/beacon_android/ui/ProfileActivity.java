package com.example.beacon_android.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.beacon_android.R;
import com.example.beacon_android.model.User;
import com.example.beacon_android.service.GenericCallback;
import com.example.beacon_android.service.UserServiceImp;

public class ProfileActivity extends AppCompatActivity implements GenericCallback {

    private User user;

    private UserServiceImp userServiceImp;

    private SharedPreferences sharedPreferences;

    private boolean isProfileEdit = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        getUser();


        findViewById(R.id.btn_editProfile).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editUser();
            }
        });

        findViewById(R.id.btn_editProfileSave).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateUser();
            }
        });

    }




    private void getUser() {

        userServiceImp = new UserServiceImp(this);
        userServiceImp.getUserProfile(getToken());

    }

    private void editUser() {
        isProfileEdit = !isProfileEdit;

        TextView lblEmail = (TextView) findViewById(R.id.lbl_profileEmail);
        TextView lblFullName = (TextView) findViewById(R.id.lbl_profileFullname);

        EditText txtEmail = (EditText) findViewById(R.id.txt_emailEditProfile);
        EditText txtFullName = (EditText) findViewById(R.id.txt_fullNameEditProfile);
        EditText txtPassword = (EditText) findViewById(R.id.txt_passwordEditProfile);
        Button btnSave = (Button) findViewById(R.id.btn_editProfileSave);


        if (isProfileEdit) {

            lblEmail.setVisibility(View.INVISIBLE);
            lblFullName.setVisibility(View.INVISIBLE);

            txtEmail.setVisibility(View.VISIBLE);
            txtFullName.setVisibility(View.VISIBLE);
            txtPassword.setVisibility(View.VISIBLE);
            btnSave.setVisibility(View.VISIBLE);


        } else {
            lblEmail.setVisibility(View.VISIBLE);
            lblFullName.setVisibility(View.VISIBLE);

            txtEmail.setVisibility(View.INVISIBLE);
            txtFullName.setVisibility(View.INVISIBLE);
            txtPassword.setVisibility(View.INVISIBLE);
            btnSave.setVisibility(View.INVISIBLE);
        }


    }

    private void updateUser() {

        EditText txtEmail = (EditText) findViewById(R.id.txt_emailEditProfile);
        EditText txtFullName = (EditText) findViewById(R.id.txt_fullNameEditProfile);
        EditText txtPassword = (EditText) findViewById(R.id.txt_passwordEditProfile);

        user.setEmail(txtEmail.getText().toString());
        user.setFullName(txtFullName.getText().toString());
        user.setPassword(txtPassword.getText().toString());

        userServiceImp = new UserServiceImp(this);
        userServiceImp.updateUser(getToken(), user);


    }


    @Override
    public void success(Object result) {

        user = (User) result;

        TextView lblEmail = (TextView) findViewById(R.id.lbl_profileEmail);


        TextView lblFullName = (TextView) findViewById(R.id.lbl_profileFullname);
        TextView txtProfileEventsCount = (TextView) findViewById(R.id.txt_profileEventsCount);

        EditText txtEmail = (EditText) findViewById(R.id.txt_emailEditProfile);
        EditText txtFullName = (EditText) findViewById(R.id.txt_fullNameEditProfile);

        lblEmail.setText("Email: " + user.getEmail());
        lblFullName.setText("Fullname: " + user.getFullName());

        txtEmail.setText(user.getEmail());
        txtFullName.setText(user.getFullName());

        if (user.getEvents() != null) {
            txtProfileEventsCount.setText(user.getEvents().size() + "\nevents");
        }

        if(isProfileEdit){
            editUser();
        }

    }

    private String getToken() {
        sharedPreferences = getApplicationContext().getSharedPreferences("MyPref", 0);
        return sharedPreferences.getString("token", null);
    }
}
