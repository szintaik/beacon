package com.example.beacon_android.service.recycleview;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.example.beacon_android.R;
import com.example.beacon_android.model.Event;

import java.util.List;

public class EventListAdapter extends RecyclerView.Adapter<EventListAdapter.MyViewHolder> {

    private List<Event> events;

    private RecycleViewCallback callback;

    private Context context;

    public EventListAdapter(List<Event> events, RecycleViewCallback callback, Context context) {
        this.events = events;
        this.callback = callback;
        this.context = context;
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView textView;
        RelativeLayout relativeLayout;

        MyViewHolder(View itemView) {
            super(itemView);
            this.imageView = (ImageView) itemView.findViewById(R.id.eventListIcon);
            this.textView = (TextView) itemView.findViewById(R.id.txtEventListName);
            relativeLayout = (RelativeLayout) itemView.findViewById(R.id.lstEventRelativeLayout);
        }
    }


    @NonNull
    @Override
    public EventListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.event_list_item, parent, false);
        EventListAdapter.MyViewHolder viewHolder = new MyViewHolder(listItem);
        return viewHolder;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(@NonNull EventListAdapter.MyViewHolder holder, int position) {
        final Event event = events.get(position);
        holder.textView.setText(event.getName() + ", " + event.getPlace());
        if (event.getLogoSrc() != null) {
            holder.imageView.setBackground(setBackground(event));
        }
        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callback.onClickEvent(event);
            }
        });
    }


    @Override
    public int getItemCount() {
        return events.size();
    }

    private Drawable setBackground(Event event) {


        byte[] valueBytes = Base64.decode(event.getLogoSrc(), Base64.DEFAULT);

        Bitmap bitmap = BitmapFactory.decodeByteArray(valueBytes, 0, valueBytes.length);


        BitmapDrawable bitmapDrawable = new BitmapDrawable(context.getResources(), bitmap);

        return bitmapDrawable;

    }

}
