package com.example.beacon_android.ui;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Base64;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.beacon_android.R;
import com.example.beacon_android.model.Beacon;
import com.example.beacon_android.model.Column;
import com.example.beacon_android.model.ColumnType;
import com.example.beacon_android.model.Row;
import com.example.beacon_android.model.Value;
import com.example.beacon_android.service.recycleview.EventListAdapter;
import com.example.beacon_android.service.recycleview.RecycleViewCallback;
import com.example.beacon_android.service.recycleview.SheetListAdapter;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class SheetActivity extends AppCompatActivity implements RecycleViewCallback {

    private Beacon beacon;
    private List<String> valueRows = new ArrayList<>();
    private RecyclerView recyclerViewRow;

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sheet);

        getBeacon();
        toolbarClickEvents();
    }


    @SuppressLint("ResourceAsColor")
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private void getBeacon() {

        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("MyPref", 0);
        Gson gson = new Gson();
        String json = sharedPreferences.getString("beacon", "");
        beacon = gson.fromJson(json, Beacon.class);


        TextView txt;
        ImageView img;

        List<Row> rows = new ArrayList<>();
        Row row;

        TableRow.LayoutParams paramsImage = new TableRow.LayoutParams(180, 180);

        for (int i = 0; i < beacon.getSheet().getColumns().get(0).getValues().size(); i++) {
            row = new Row();

            for (int a = 0; a < beacon.getSheet().getColumns().size(); a++) {

                if (beacon.getSheet().getColumns().get(a).getType() == ColumnType.TEXT) {

                    txt = new TextView(this);
                    txt.setText(beacon.getSheet().getColumns().get(a).getValues().get(i).getValue());
                    txt.setTextSize(18);
                    row.getTexts().add(txt);

                }
                if (beacon.getSheet().getColumns().get(a).getType() == ColumnType.IMAGE) {

                    img = new ImageView(this);
                    paramsImage.leftMargin = 10;
                    paramsImage.rightMargin = 10;
                    img.setLayoutParams(paramsImage);


                    byte[] valueBytes = Base64.decode(beacon.getSheet().getColumns().get(a).getValues().get(i).getValue(), Base64.DEFAULT);

                    Bitmap bitmap = BitmapFactory.decodeByteArray(valueBytes, 0, valueBytes.length);

                    BitmapDrawable bitmapDrawable = new BitmapDrawable(getResources(), bitmap);

                    img.setBackground(bitmapDrawable);

                    row.getImages().add(img);

                }


            }
            rows.add(row);
        }


        recyclerViewRow = (RecyclerView) findViewById(R.id.recycleView_sheet);
        SheetListAdapter adapter = new SheetListAdapter(rows, this);
        recyclerViewRow.setHasFixedSize(true);
        recyclerViewRow.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewRow.setAdapter(adapter);


    }


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onClickEvent(Object result) {

        ImageView image = (ImageView) result;
        ImageView imageCopy = new ImageView(this);

        imageCopy.setBackground(image.getBackground());

        AlertDialog.Builder imageBuilder = new AlertDialog.Builder(SheetActivity.this);

        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setOrientation(LinearLayout.HORIZONTAL);
        linearLayout.addView(imageCopy);

        imageBuilder.setView(linearLayout);


        imageBuilder
                .setCancelable(true);


        AlertDialog alertDialog = imageBuilder.create();


        alertDialog.show();

    }


    private void toolbarClickEvents() {

        final ImageView btnLogout = (ImageView) findViewById(R.id.btn_logoutToolbar);
        final ImageView btnBack = (ImageView) findViewById(R.id.btn_backToolbar);

        btnLogout.setVisibility(View.INVISIBLE);
        ((ImageView) findViewById(R.id.btn_profileToolbar)).setVisibility(View.INVISIBLE);
        btnBack.setVisibility(View.VISIBLE);

        final Activity thisActivity = this;

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                thisActivity.finish();
            }
        });
    }

}
