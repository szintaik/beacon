package com.example.beacon_android.service;

import com.example.beacon_android.model.JwtRequest;
import com.example.beacon_android.model.JwtResponse;
import com.example.beacon_android.model.User;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;

public interface UserService {

    @POST("authenticate")
    Call<JwtResponse> login(@Body JwtRequest jwtRequest);

    @POST("user/registration")
    Call<User> registration(@Body User user);

    @GET("user/profile")
    Call<User> getUserProfile(@Header("Authorization") String token);

    @PUT("user?password=true")
    Call<User> updateUser(@Header("Authorization") String token, @Body User user);
}



