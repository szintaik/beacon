package com.example.beacon_android.model;

import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class Row {

    private List<ImageView> images = new ArrayList<>();
    private List<TextView> texts = new ArrayList<>();

    public Row() {
    }


    public List<ImageView> getImages() {
        return images;
    }

    public void setImages(List<ImageView> images) {
        this.images = images;
    }

    public List<TextView> getTexts() {
        return texts;
    }

    public void setTexts(List<TextView> texts) {
        this.texts = texts;
    }
}
